""" think of this (__init__.py) file as a constructor 
    that pulls all of the parts of application together 
    into a package and then tells Python to treat ut as a pachage !

"""

from flask import Flask

from flask_mysqldb import MySQL


app = Flask(__name__)

# Config MySQL
# following pattern alows us to control the config environment outside the application
#  by setting the FLASK_ENV environment variable in the terminal

if app.config["ENV"] == "production":
    app.config.from_object("config.ProductionConfig")
elif app.config["ENV"] == "testing":
    app.config.from_object("config.TestingConfig")
else:
    app.config.from_object("config.DevelopmentConfig")
app.config['CKEDITOR_PKG_TYPE'] = 'basic'

mysql = MySQL(app)


# import the blueprints and register them
from app.views.public_views import public_views   # nopep8
app.register_blueprint(public_views, url_prefix="")
from app.views.admin_views import admin_views   # nopep8
app.register_blueprint(admin_views, url_prefix="")
from app.views.dashboard_views import dashboard_views   # nopep8
app.register_blueprint(dashboard_views, url_prefix="")
from app.views.latex import latex   # nopep8
app.register_blueprint(latex, url_prefix="")

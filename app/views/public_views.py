

from flask import Blueprint, render_template, request, flash, redirect, url_for, abort
from jinja2 import TemplateNotFound


# a blueprint is an concept in Flask that helps make the applicatios more modular.
public_views = Blueprint('public_views', __name__, static_folder="static",
                         template_folder="templates")


@public_views.route('/')
def index():

    if request.method == 'GET':
        return render_template("public/home.html")


# about
@public_views.route('/about')
def about():
    return render_template("public/about.html")

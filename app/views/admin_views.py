from flask import render_template, request, flash, redirect, url_for, logging, session
from wtforms import BooleanField, Form, IntegerField, PasswordField, StringField, TextAreaField, validators, widgets
from wtforms.fields.html5 import DateField, DateTimeLocalField
import datetime
from wtforms.validators import DataRequired, Email, InputRequired, Required
from passlib.hash import sha256_crypt
from functools import wraps
import os
import re
from wtforms.fields.core import SelectField, SelectMultipleField
from flask.blueprints import Blueprint
from flask import current_app
from flask.json import jsonify
from flask.helpers import make_response

from flask_admin import Admin

# import mysql from main app
from app import mysql

admin_views = Blueprint('admin_views', __name__, static_folder="static",
                        template_folder="templates")


# Check User Is Admin
def is_admin():
    cur = mysql.connection.cursor()
    # Execute
    result = cur.execute(
        "SELECT  * FROM users WHERE id=%s", [session["id"]])
    user = cur.fetchall()
    # close connection
    cur.close()
    return user[0]["is_admin"]


# Register Form Class
class RegisterForm(Form):
    first_name = StringField('First Name', [validators.Length(min=1, max=100)])
    last_name = StringField('Last Name', [validators.Length(min=1, max=100)])
    email = StringField('Email',  validators=[DataRequired(), Email()])
    dept = StringField('Department', [validators.Length(min=1, max=100)])
    password = PasswordField('Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Passwords do not match')
    ])
    confirm = PasswordField('Confirm Password')


# User Register
@admin_views.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm(request.form)
    if request.method == 'POST' and form.validate():
        first_name = form.first_name.data
        last_name = form.last_name.data
        email = form.email.data
        dept = form.dept.data
        password = sha256_crypt.encrypt(str(form.password.data))

        # Create cursor
        cur = mysql.connection.cursor()
        # check if email exists in MySql
        cur.execute("SELECT * FROM users WHERE email =%s ", [email])
        emailExists = cur.fetchone()

        # just uni jena email address acceptable
        if not re.match(r"\w+\.\w+@uni\-jena\.de", email):
            flash("You can register just with Uni Jena Mail address", "danger")
            # Close connection
            cur.close()

        # if email address exists show an error
        elif emailExists:
            flash("The Email Address already exists! ", "danger")
            # Close connection
            cur.close()

        # Execute query
        else:
            cur.execute("INSERT INTO users(first_name, last_name, email, dept, password) VALUES(%s, %s, %s, %s, %s)",
                        (first_name, last_name, email, dept, password))

            user_id = cur.lastrowid

            # User Access level
            cur.execute(
                f"INSERT INTO access_premission(user_id) VALUES({user_id})")

            # Commit to DB
            mysql.connection.commit()

            # Close connection
            cur.close()

            flash('You are now registered and can log in', 'success')
            return redirect(url_for('admin_views.login'))

    return render_template('admin/register.html', form=form)


# User login
@admin_views.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        # Get Form Fields
        email = request.form['email']
        password_candidate = request.form['password']

        # Create cursor
        cur = mysql.connection.cursor()

        # Get user by email
        result = cur.execute(
            "SELECT * FROM users WHERE email = %s", [email])
        # Get stored hash
        data = cur.fetchone()
        # Close connection
        cur.close()

        if result > 0:
            password = data['password']
            first_name = data["first_name"]
            id = data["id"]
            admin = data["is_admin"]

            # Compare Passwords
            if sha256_crypt.verify(password_candidate, password):
                # Fist clear session just for secure
                session.clear()

                # Passed
                session['logged_in'] = True
                session['first_name'] = first_name
                session["id"] = id
                if admin == 1:
                    session["is_admin"] = True

                flash('You are now logged in', 'success')
                return redirect(url_for('dashboard_views.dashboard'))
            else:
                error = 'Invalid login'
                return render_template('admin/login.html', error=error)
        else:
            error = 'Email not found'
            return render_template('admin/login.html', error=error)

    return render_template('admin/login.html')


# Check if user logged in
def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if "logged_in" in session:
            return f(*args, **kwargs)
        else:
            flash("Unauthorized, Please login", "danger")
            return redirect(url_for('admin_views.login'))
    return wrap


# Logout
@admin_views.route('/logout')
@is_logged_in
def logout():
    session.clear()
    flash("You are now logged out", "success")
    return redirect(url_for("admin_views.login"))


# Admin Dashobard
@admin_views.route("/admin")
@is_logged_in
def admin_dashboard():
    if is_admin():
        # Create cursor
        cur = mysql.connection.cursor()

        # Get aufgaben
        result = cur.execute("SELECT * FROM users")

        users = cur.fetchall()

        cur.close()

        return render_template("admin/admin_dashboard.html", users=users)
    else:
        flash("You don't have admin Privilege", "danger")
        return redirect(url_for("dashboard_views.dashboard"))


# Delete a user
@admin_views.route('/admin/delete_user/<int:id>', methods=['POST'])
@is_logged_in
def delete_user(id):

    # Create cursor
    cur = mysql.connection.cursor()

    # Execute
    cur.execute("DELETE FROM users WHERE id = %s", [id])

    # Commit to DB
    mysql.connection.commit()

    # Close connection
    cur.close()

    flash('User Deleted', 'success')

    return redirect(url_for('admin_views.admin_dashboard'))


# Make a user admin
@admin_views.route('/make_admin/<int:id>', methods=['POST'])
@is_logged_in
def make_admin(id):

    if request.method == 'POST':
        # Create cursor
        cur = mysql.connection.cursor()

        # Get Aufgabe by id
        result = cur.execute("SELECT is_admin FROM users WHERE id = %s", [id])

        user = cur.fetchone()

        is_admin = int(not user["is_admin"])

        # Execute
        cur.execute(
            "UPDATE users SET is_admin=%s WHERE id=%s", (is_admin, id))
        # Commit to DB
        mysql.connection.commit()

        # Close connection
        cur.close()

        flash('User Updated', 'success')

        return redirect(url_for('admin_views.admin_dashboard'))


# User's Access Level
@admin_views.route("/admin/aufgaben_access")
@is_logged_in
def aufgaben_access():
    # create curser
    cur = mysql.connection.cursor()

    # Execute
    result = cur.execute("SELECT  * FROM access_premission")

    users = cur.fetchall()
    # close connection
    cur.close()

    res = make_response(jsonify(users), 200)
    return res


# Update User Access Level
@admin_views.route("/admin/update_access_level", methods=["POST"])
@is_logged_in
def upadate_access_level():

    req = request.get_json()
    user_id = req["user_id"]
    access_level = req["access_level"]

    # Create Cursor
    cur = mysql.connection.cursor()

    # Execute
    cur.execute("UPDATE access_premission SET access_level=%s WHERE user_id=%s",
                (access_level, user_id))

    # Commit to DB
    mysql.connection.commit()

    # Close Connection
    cur.close()

    res = make_response(jsonify("Ok"), 200)
    return res

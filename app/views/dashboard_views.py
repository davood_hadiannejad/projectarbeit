from flask import render_template, request, flash, redirect, url_for, logging, session
from wtforms import BooleanField, Form, IntegerField, PasswordField, StringField, TextAreaField, validators, widgets
from wtforms.fields.html5 import DateField, DateTimeLocalField
import datetime
from wtforms.validators import DataRequired, InputRequired, Required
from passlib.hash import sha256_crypt
from functools import wraps


import os
import re
from wtforms.fields.core import SelectField, SelectMultipleField
from app import mysql

from flask.blueprints import Blueprint
from flask import current_app
from flask.json import jsonify
from flask.helpers import make_response
import bleach

# import the function if an user logged in
from app.views.admin_views import is_logged_in

dashboard_views = Blueprint('dashboard_views', __name__, static_folder="static",
                            template_folder="templates")


# Check User Acces Level
def access_level():

    cur = mysql.connection.cursor()

    # Execute
    result = cur.execute(
        "SELECT  * FROM access_premission WHERE user_id=%s", [session["id"]])

    user = cur.fetchall()

    # close connection
    cur.close()

    return user[0]["access_level"]


# Dashboard
@dashboard_views.route("/dashboard", methods=["GET", "POST"])
@is_logged_in
def dashboard():

    # Create Cursor
    cur = mysql.connection.cursor()

    # Get user's Dashboard
    result = cur.execute(
        "SELECT * FROM Veranstaltung  INNER JOIN dashboard ON dashboard.veranSt_id = Veranstaltung.ID WHERE dashboard.user_id =%s ORDER BY Veranstaltung.Titel", [session["id"]])

    veranstaltungen = cur.fetchall()

    # Close Connection
    cur.close()

    return render_template("dashboard/dashboard.html", veranSt=veranstaltungen, access_level=access_level())


# Veranstaltungen Form Class
class VeranStForm(Form):
    Stil = IntegerField("Stil", [validators.NumberRange(min=0)])
    Verantwortlich = StringField(
        "Verantwortlich", [validators.length(min=1, max=255)])
    Mitarbeiter = StringField(
        "Mitarbeiter", [validators.length(min=1, max=255)])
    Titel = StringField(
        "Titel", [validators.length(min=1, max=255)])
    Kopftext = TextAreaField(
        "Kopftext", [validators.Length(min=1, max=255)])
    Fusstext = TextAreaField(
        "Fusstext", [validators.Length(min=1, max=255)])
    Kurztext = TextAreaField(
        "Kurztext", [validators.Length(min=1, max=20)])


# Add Verranstaltung
@ dashboard_views.route("/add_veranSt", methods=["GET", "POST"])
@ is_logged_in
def add_veranstaltung():
    if access_level() == "full_access":
        form = VeranStForm(request.form)
        if request.method == "POST" and form.validate():
            Stil = form.Stil.data
            Verantwortlich = form.Verantwortlich.data
            Mitarbeiter = form.Mitarbeiter.data
            Titel = form.Titel.data
            Kopftext = form.Kopftext.data
            Fusstext = form.Fusstext.data
            Kurztext = form.Kurztext.data

            # Create Curser
            cur = mysql.connection.cursor()

            # Execute
            cur.execute("INSERT INTO Veranstaltung( Stil, Verantwortlich,Mitarbeiter, Titel, Kopftext,Fusstext,Kurztext ) VALUES(%s, %s, %s,%s, %s,%s, %s)",
                        (Stil,  Verantwortlich, Mitarbeiter, Titel, Kopftext, Fusstext, Kurztext))
            # Get the created Veranstaltung's id
            veranSt_id = cur.lastrowid

            # Add the user id and veranSt_id into dashboard table
            cur.execute("INSERT INTO dashboard(user_id, veranSt_id, is_owner) VALUES(%s,%s, %s)",
                        (session["id"], veranSt_id, 1))

            # Commit to DB
            mysql.connection.commit()

            # Close connection
            cur.close()

            flash("Veranstaltung Created", "success")

            return redirect(url_for("dashboard_views.dashboard"))

        return render_template("dashboard/add_veranSt.html", form=form)
    else:
        return redirect(url_for("dashboard_views.dashboard"))


# Get Veranstaltung's users
@dashboard_views.route("dashboard/veranstaltungs/users/<int:id>", methods=["GET", "POST"])
@is_logged_in
def veranstalung_users(id):
    if access_level() != "no_access":
        # Create curser
        cur = mysql.connection.cursor()

        # Check if user is own of the lecture
        result = cur.execute("SELECT * FROM dashboard  WHERE user_id=%s AND veranSt_id=%s", [
            session["id"], id])

        user = cur.fetchone()

        if result > 0:
            # Execute
            cur.execute(
                "SELECT users.first_name, users.last_name,users.email, users.dept, dashboard.user_id, dashboard.veranSt_id FROM users RIGHT JOIN dashboard ON users.id = dashboard.user_id WHERE veranSt_id =%s ", [id])
            veranSt_users = cur.fetchall()
            # Close cursor
            cur.close()

            return render_template('dashboard/veranSt_users.html', veranSt_users=veranSt_users, veranSt_id=id, access_level=access_level())
        else:
            # Close connection
            cur.close()
            flash("Unauthorized, Please login", "danger")
            return redirect(url_for("dashboard_views.dashboard"))
    return redirect(url_for("dashboard_views.dashboard"))


# Get the users for selecting them and assinging them to a lecture
@dashboard_views.route("/dashboard/veranstaltungs/users/all", methods=["GET"])
@is_logged_in
def all_users():
    # create curser
    cur = mysql.connection.cursor()

    # Execute
    result = cur.execute("SELECT * FROM users")

    users = cur.fetchall()

    # close connection
    cur.close()

    if result > 0:
        res = make_response(jsonify(users), 200)
        return res


# Assign a user to an Veranstaltung
@dashboard_views.route("/dashboard/veranstaltungs/users/assign", methods=["POST"])
@is_logged_in
def assign_users():
    req = request.get_json()
    if access_level() == "full_access":
        if request.method == "POST":

            user_id = req["user_id"]
            veranSt_id = req["veranSt_id"]

            # Create Curser
            cur = mysql.connection.cursor()

            # Check if user owner is
            cur.execute("SELECT is_owner FROM dashboard WHERE user_id=%s AND veranSt_id=%s", [
                        session["id"], veranSt_id])

            user = cur.fetchone()

            if user["is_owner"] == 1:
                cur.execute(
                    "INSERT INTO dashboard(user_id, veranSt_id) VALUES(%s,%s)", (user_id, veranSt_id))

                # Commit to DB
                mysql.connection.commit()

                flash("User Added ", "success")
            else:
                flash(
                    "Permission Denied, You are not the owner of this lecture", "danger")

            # Close connection
            cur.close()

            res = make_response(jsonify("Ok", 200))
            return res
    else:
        res = make_response(jsonify("Not Fund", 404))
        return res


# Delete a user from a lecture
@dashboard_views.route("dashboard/veranstaltungs/users/delete/<int:user_id>/<int:veranSt_id>", methods=["POST"])
@is_logged_in
def delete_veranSt_user(user_id, veranSt_id):

    # Create Curser
    cur = mysql.connection.cursor()

    # Check if user owner is
    cur.execute("SELECT * FROM dashboard WHERE user_id=%s AND veranSt_id=%s", [
                session["id"], veranSt_id])

    user = cur.fetchone()

    if user["user_id"] == user_id:
        flash("Permission Denied, You can not delete yourself, please go to your dashboard and delete the lecture", "danger")
        return redirect(url_for("dashboard_views.veranstalung_users", id=veranSt_id))
        # close connection
        cur.close()

    elif user["is_owner"] == 1:
        # Execute
        cur.execute("DELETE FROM dashboard WHERE user_id=%s AND veranSt_id=%s", [
                    user_id, veranSt_id])

        # Commit to DB
        mysql.connection.commit()

        # Close Connection
        cur.close

        flash("User Deleted", "success")

        return redirect(url_for("dashboard_views.veranstalung_users", id=veranSt_id))

    else:
        # close connection
        cur.close()
        flash("Permission Denied, You are not the owner of this lecture", "danger")
        return redirect(url_for("dashboard_views.veranstalung_users", id=veranSt_id))


# Edit Veranstaltung
@ dashboard_views.route('/edit_veranSt/<string:ID>', methods=['GET', 'POST'])
@ is_logged_in
def edit_veranSt(ID):
    if access_level() == "full_access":
        # Create cursor
        cur = mysql.connection.cursor()

        result = cur.execute("SELECT * FROM dashboard  WHERE user_id=%s AND veranSt_id=%s", [
            session["id"], ID])

        user = cur.fetchone()

        if result > 0:

            # Get Veranstaltung by id
            result = cur.execute(
                "SELECT * FROM Veranstaltung WHERE ID = %s", [ID])

            veranSt = cur.fetchone()

            # Close the connection
            cur.close()

            # Get form
            form = VeranStForm(request.form)

            # Populate Veranstaltung form fields
            form.Stil.data = veranSt['Stil']
            form.Verantwortlich.data = veranSt['Verantwortlich']
            form.Mitarbeiter.data = veranSt['Mitarbeiter']
            form.Titel.data = veranSt['Titel']
            form.Kopftext.data = veranSt['Kopftext']
            form.Fusstext.data = veranSt['Fusstext']
            form.Kurztext.data = veranSt['Kurztext']

            if request.method == 'POST' and form.validate():
                Stil = request.form['Stil']
                Verantwortlich = request.form['Verantwortlich']
                Mitarbeiter = request.form['Mitarbeiter']
                Titel = request.form['Titel']
                Kopftext = request.form['Kopftext']
                Fusstext = request.form['Fusstext']
                Kurztext = request.form['Kurztext']

                # Create Cursor
                cur = mysql.connection.cursor()
                current_app.logger.info(Titel)

                # Execute
                cur.execute(
                    "UPDATE Veranstaltung SET Stil=%s, Verantwortlich=%s,  Mitarbeiter=%s, Titel=%s, Kopftext=%s, Fusstext=%s, Kurztext=%s WHERE ID=%s", (Stil, Verantwortlich,  Mitarbeiter, Titel, Kopftext, Fusstext, Kurztext, ID))

                # Commit to DB
                mysql.connection.commit()

                # Close connection
                cur.close()

                flash('Veranstaltung Updated', 'success')

                return redirect(url_for('dashboard_views.dashboard'))

            return render_template('dashboard/edit_veranSt.html', form=form)
        else:
            # Close connection
            cur.close()
            flash("Permission Denied, You are not the owner of this lecture", "danger")
            return redirect(url_for("dashboard_views.dashboard"))
    else:
        return redirect(url_for("dashboard_views.dashboard"))


# Delete Vetanstaltung
@ dashboard_views.route('/delete_veranSt/<string:ID>', methods=['POST'])
@ is_logged_in
def delete_veranSt(ID):

    # Create cursor
    cur = mysql.connection.cursor()

    # Execute
    cur.execute("SELECT * FROM dashboard  WHERE user_id=%s AND veranSt_id=%s", [
                session["id"], ID])

    user = cur.fetchone()

    if user["is_owner"] == 1:

        # Execute
        cur.execute("DELETE FROM Veranstaltung WHERE ID = %s", [ID])

        # Commit to DB
        mysql.connection.commit()

        flash('Veranstaltung Deleted', 'success')

    else:
        flash("Permission Denied, You are not the owner of this lecture", "danger")

    # Close connection
    cur.close()

    return redirect(url_for('dashboard_views.dashboard'))


# Dashboard Aufgaben
@dashboard_views.route("/aufgaben_dashboard")
@is_logged_in
def aufgaben_dashboard():
    # Create cursor
    cur = mysql.connection.cursor()

    # Get aufgaben
    result = cur.execute("SELECT * FROM Aufgaben")

    aufgaben = cur.fetchall()

    cur.close()
    if result > 0:
        return render_template("dashboard/aufgaben_dashboard.html", aufgaben=aufgaben, access_level=access_level())
    else:
        flash("No Aufgabe Found", "danger")
        return redirect(url_for('dashboard_views.aufgaben_dashboard'))


# Single Aufgabe
@dashboard_views.route('/aufgabe/<int:ID>/')
@is_logged_in
def aufgabe(ID):
    if access_level() != "no_access":
        # Create cursor
        cur = mysql.connection.cursor()

        # Get aufgabe
        result = cur.execute("SELECT * FROM Aufgaben WHERE ID = %s", [ID])

        aufgabe = cur.fetchone()
        cur.close()
        return render_template('dashboard/aufgabe.html', aufgabe=aufgabe)
    return redirect(url_for('dashboard_views.aufgaben_dashboard'))


# Aufgabe Form Class
class AufgabeForm(Form):
    Name = StringField(
        "Name", validators=[DataRequired()])
    Titel = StringField(
        "Titel", validators=[DataRequired()])
    Gebiet = StringField(
        "Gebiet", [validators.Length(min=1, max=40)])
    version = IntegerField("version", [validators.NumberRange(min=0)])
    Aufgabe = TextAreaField(
        "Aufgabe", validators=[DataRequired()])
    Loesung = TextAreaField(
        "Loesung", validators=[DataRequired()])


# Add Aufgabe
@dashboard_views.route("/add_aufgabe", methods=["GET", "POST"])
@is_logged_in
def add_aufgabe():
    if access_level() == "full_access":
        form = AufgabeForm(request.form)
        if request.method == "POST" and form.validate():
            Name = form.Name.data
            Titel = form.Titel.data
            Gebiet = form.Gebiet.data
            version = form.version.data
            Aufgabe = form.Aufgabe.data
            Loesung = form.Loesung.data

            # Create Curser
            cur = mysql.connection.cursor()

            # Execute
            cur.execute("INSERT INTO Aufgaben(Name, Titel, Gebiet, version, Aufgabe,Loesung) VALUES(%s, %s, %s,%s, %s, %s)",
                        (Name, Titel, Gebiet, version,  Aufgabe,  Loesung))

            # Commit to DB
            mysql.connection.commit()

            # Close connection
            cur.close()

            flash("Aufgabe Created", "success")

            return redirect(url_for("dashboard_views.aufgaben_dashboard"))

        return render_template("dashboard/add_aufgabe.html", form=form)

    return redirect(url_for('dashboard_views.dashboard'))


# Edit Aufgabe
@dashboard_views.route('/edit_aufgabe/<string:ID>', methods=['GET', 'POST'])
@is_logged_in
def edit_aufgabe(ID):
    if access_level() == "full_access":
        # Create cursor
        cur = mysql.connection.cursor()

        # Get Aufgabe by id
        result = cur.execute("SELECT * FROM Aufgaben WHERE ID = %s", [ID])

        aufgabe = cur.fetchone()
        cur.close()
        # Get form
        form = AufgabeForm(request.form)

        # Populate aufgabe form fields
        form.Name.data = aufgabe['Name']
        form.Titel.data = aufgabe['Titel']
        form.Gebiet.data = aufgabe['Gebiet']
        form.version.data = aufgabe['version']
        form.Aufgabe.data = aufgabe['Aufgabe']
        form.Loesung.data = aufgabe['Loesung']

        if request.method == 'POST' and form.validate():
            Name = request.form['Name']
            Titel = request.form['Titel']
            Gebiet = request.form['Gebiet']
            version = request.form['version']
            Aufgabe = request.form['Aufgabe']
            Loesung = request.form['Loesung']

            # Create Cursor
            cur = mysql.connection.cursor()
            current_app.logger.info(Titel)

            # Execute
            cur.execute(
                "UPDATE Aufgaben SET Name=%s,  Titel=%s, Gebiet=%s, version=%s, Aufgabe=%s, Loesung=%s WHERE ID=%s", (Name, Titel, Gebiet, version, Aufgabe, Loesung, ID))
            # Commit to DB
            mysql.connection.commit()

            # Close connection
            cur.close()

            flash('Aufgabe Updated', 'success')

            return redirect(url_for('dashboard_views.aufgaben_dashboard'))

        return render_template('dashboard/edit_aufgabe.html', form=form)

    return redirect(url_for('dashboard_views.dashboard'))


# Delete Aufgabe
@dashboard_views.route('/delete_aufgabe/<string:ID>', methods=['POST'])
@is_logged_in
def delete_aufgabe(ID):
    # Create cursor
    cur = mysql.connection.cursor()

    # Execute
    cur.execute("DELETE FROM Aufgaben WHERE ID = %s", [ID])

    # Commit to DB
    mysql.connection.commit()

    # Close connection
    cur.close()

    flash('Aufgabe Deleted', 'success')

    return redirect(url_for('dashboard_views.aufgaben_dashboard'))


#  Veranstaltung's Blatts
@dashboard_views.route("/dashboard/veranstaltungs/blatt/<int:id>", methods=["GET"])
@is_logged_in
def blatt(id):
    if access_level() != "no_access":
        # Create cursor
        cur = mysql.connection.cursor()

        # Check if user is own of the lecture
        result = cur.execute("SELECT * FROM dashboard  WHERE user_id=%s AND veranSt_id=%s", [
            session["id"], id])

        user = cur.fetchone()

        if result > 0:

            # Get Blatts
            result = cur.execute(
                "SELECT * from Blatt WHERE Veranstaltung = %s", [id])

            blatt = cur.fetchall()

            # Close connection
            cur.close()

            if result > 0:
                return render_template("dashboard/blatt.html", blatt=blatt, access_level=access_level())
            else:

                if access_level() == "full_access":
                    flash("No Blatt Found, Please Add Your Frist Blatt", "danger")
                    return render_template("dashboard/add_blatt.html", veranSt=id)

                flash("No Blatt Found And You Are Just In Read Mode", "danger")
                return redirect(url_for('dashboard_views.dashboard'))
        else:
            # Close connection
            cur.close()

            flash("Permission Denied, You are not the owner of this lecture", "danger")
            return redirect(url_for("dashboard_views.dashboard"))
    return redirect(url_for('dashboard_views.dashboard'))


def month_number_to_stting(number):
    m = {
        1: "Jan",
        2: "Feb",
        3: "März",
        4: "Apr",
        5: "Mai",
        6: "Jun",
        7: "Juli",
        8: "Aug",
        9: "Sept",
        10: "Okt",
        11: "Nov",
        12: "Dez"
    }
    try:
        out = m[number]
        return out
    except:
        raise ValueError("Not a month")


# add Blatt
@ dashboard_views.route("/dashboard/veranstaltungs/blatt/add/<int:veranSt>", methods=["GET", "POST"])
@ is_logged_in
def add_blatt(veranSt):
    if access_level() == "full_access":
        if request.method == "POST":

            #  Get json from forntend
            req = request.get_json()

            # Get Form Fields(josn)
            Nr = req["Nr"]
            Name = req["Name"]
            Titel = req["Titel"]
            Veranstaltung = req["Veranstaltung"]
            Ausgabe = req["Ausgabe"]
            Abgabe = req["Abgabe"]
            Zusatz = req["Zusatz"]
            Stil = req["Stil"]

            # The strftime method helped us convert date objects into more readable strings.
            # The strptime method does the opposite, that is,
            # it takes strings and converts them into date objects that Python can understand.
            # https://stackabuse.com/how-to-format-dates-in-pyt

            # strptime => String to Datetime
            # strftime => Datetime to String

            Ausgabe = datetime.datetime.strptime(
                Ausgabe, "%Y-%m-%d")
            Ausgabe = Ausgabe.strftime(
                f"%d. {month_number_to_stting(Ausgabe.month)} %Y")

            Abgabe = datetime.datetime.strptime(
                Abgabe, "%Y-%m-%d")

            Abgabe = Abgabe.strftime(
                f"%d. {month_number_to_stting(Abgabe.month)} %Y")

            # Create Curser
            cur = mysql.connection.cursor()

            # Execute to insert data into blatt table
            cur.execute("INSERT INTO Blatt( Nr, Name,  Titel, Veranstaltung,  Ausgabe, Abgabe, Zusatz, Stil ) VALUES( %s, %s, %s, %s, %s, %s, %s, %s)",
                        (Nr, Name, Titel, Veranstaltung, Ausgabe, Abgabe, Zusatz, Stil))

            id = cur.lastrowid

            blatt_id = cur.lastrowid
            addedAufgaben = req["addedAufgaben"]
            if addedAufgaben != []:
                for index in range(len(addedAufgaben)):
                    cur.execute(
                        "INSERT INTO helpers(blatt_id, aufgaben_id, point_a, order_a) VALUES(%s, %s, %s, %s)", (blatt_id, addedAufgaben[index]["id"], addedAufgaben[index]["point_a"], index+1))
                    # cur.execute("UPDATE helpers SET order_a =%s WHERE id=%s",
                    #             (cur.lastrowid, cur.lastrowid))

            # Commit to DB
            mysql.connection.commit()

            # close conection
            cur.close()

            flash("Blatt Created", "success")

            res = make_response(jsonify({"message": "OK"}), 200)
            return res

        return render_template("dashboard/add_blatt.html", veranSt=veranSt)
    return redirect(url_for('dashboard_views.dashboard'))


# Get aufgaben for selecting them to a blatt
@dashboard_views.route("/dashboard/veranstaltungs/blatt/add/aufgaben")
@ is_logged_in
def getAufgaben():
    # Create cursor
    cur = mysql.connection.cursor()

    # Get aufgaben
    result = cur.execute("SELECT * FROM Aufgaben")

    aufgaben = cur.fetchall()

    # Close connection
    cur.close()

    if result > 0:
        response = make_response(jsonify(aufgaben), 200)
        return response


# Get the Veranstaltungen for selecting them to a blatt
@dashboard_views.route("/dashboard/veranstaltungs/blatt/add/veranstaltungen")
@ is_logged_in
def getVeranstaltung():
    # Create cursor
    cur = mysql.connection.cursor()

    # Get aufgaben
    result = cur.execute("SELECT * FROM Veranstaltung")

    veranstaltung = cur.fetchall()

    # Close connection
    cur.close()

    if result > 0:
        response = make_response(jsonify(veranstaltung), 200)
        return response


def month_string_to_number(string):
    m = {
        'Jan': 1,
        'Feb': 2,
        'März': 3,
        'Apr': 4,
        'Mai': 5,
        'Jun': 6,
        'Juli': 7,
        'Aug': 8,
        'Sept': 9,
        'Okt': 10,
        'Nov': 11,
        'Dez': 12
    }

    try:
        out = m[string]
        return out
    except:
        raise ValueError('Not a month')


# Edit Blatt
# @ dashboard_views.route('/dashboard/veranstaltungs/blatt/edit', methods=['POST'])
@ dashboard_views.route('/dashboard/veranstaltungs/blatt/edit/<int:veranSt>/<string:ID>', methods=['GET', "POST"])
@ is_logged_in
def edit_blatt(veranSt, ID):
    if access_level() == "full_access":
        if request.method == "POST":

            #  Get json from forntend
            req = request.get_json()

            # Get Form Fields(josn)
            Nr = req["Nr"]
            Name = req["Name"]
            Titel = req["Titel"]
            Veranstaltung = req["Veranstaltung"]
            Ausgabe = req["Ausgabe"]
            Abgabe = req["Abgabe"]
            Zusatz = req["Zusatz"]
            Stil = req["Stil"]
            BlattId = req["BlattId"]
            addedAufgaben = req["addedAufgaben"]

            Ausgabe = datetime.datetime.strptime(
                Ausgabe, "%Y-%m-%d")
            Ausgabe = Ausgabe.strftime(
                f"%d. {month_number_to_stting(Ausgabe.month)} %Y")

            Abgabe = datetime.datetime.strptime(
                Abgabe, "%Y-%m-%d")

            Abgabe = Abgabe.strftime(
                f"%d. {month_number_to_stting(Abgabe.month)} %Y")

            # Create Cursor
            cur = mysql.connection.cursor()

            current_app.logger.info(Titel)

            # Execute
            cur.execute(
                "UPDATE Blatt SET Nr=%s, Name=%s, Titel=%s, Ausgabe=%s, Abgabe=%s, Zusatz=%s, Stil=%s WHERE ID=%s", (Nr, Name, Titel, Ausgabe, Abgabe, Zusatz, Stil, BlattId))

            # check if are there any aufgabe
            if addedAufgaben != []:
                # Select the aufgaben from the database
                result = cur.execute(
                    "SELECT * FROM helpers WHERE blatt_id=%s", [BlattId])
                # Fetch them
                aufgabe = cur.fetchall()

                # Loop through aufgaben in the form
                for index in range(len(addedAufgaben)):
                    # loop through aufgaben in the database
                    for i in aufgabe:
                        # is one of form aufgaben in the database, if yes update it
                        if (addedAufgaben[index]["id"]) == str(i["aufgaben_id"]):
                            cur.execute(
                                "UPDATE  helpers SET   point_a =%s, order_a=%s WHERE aufgaben_id=%s", (addedAufgaben[index]["point_a"], index+1, addedAufgaben[index]["id"]))
                            break
                    # if there is no one in the database insert it into database
                    else:
                        cur.execute(
                            "INSERT INTO helpers(blatt_id, aufgaben_id, point_a, order_a) VALUES(%s, %s, %s, %s)", (BlattId, addedAufgaben[index]["id"], addedAufgaben[index]["point_a"], index+1))

            # Commit to DB
            mysql.connection.commit()

            # Close connection
            cur.close()

            flash('Blatt Updated', 'success')
            res = make_response(jsonify({"message": "OK"}), 200)
            return res

        # Create cursor
        cur = mysql.connection.cursor()

        result = cur.execute("SELECT * FROM dashboard  WHERE user_id=%s AND veranSt_id=%s", [
            session["id"], veranSt])

        user = cur.fetchone()

        if result > 0:
            # Get Blatt by id
            result = cur.execute("SELECT * FROM Blatt WHERE ID = %s", [ID])

            blatt = cur.fetchone()
            if result > 0:
                #   convert the string date into python datetime
                Ausgabe = blatt['Ausgabe']
                d, m, y = Ausgabe.split(' ', 2)
                d = d.replace(".", "")
                m = month_string_to_number(m)
                Ausgabe = (f"{y}-{m}-{d}")
                Ausgabe = datetime.datetime.strptime(
                    Ausgabe, "%Y-%m-%d").date()

                #   convert the string date into python datetime
                Abgabe = blatt['Abgabe']
                d, m, y = Abgabe.split(' ', 2)
                d = d.replace(".", "")
                m = month_string_to_number(m)
                Abgabe = (f"{y}-{m}-{d}")
                Abgabe = datetime.datetime.strptime(Abgabe, "%Y-%m-%d").date()

                # Get form
                form = request.form

                # Populate aufgabe form fields
                form.Nr = blatt['Nr']
                form.Name = blatt['Name']
                form.Titel = blatt['Titel']
                form.Veranstaltung = blatt['Veranstaltung']
                form.Ausgabe = Ausgabe  # 2018-07-22
                form.Abgabe = Abgabe
                form.Zusatz = blatt['Zusatz']
                form.Stil = blatt['Stil']
                form.blattId = ID

                # Get Veranstaltung Titel by its ID
                veranstaltung_result = cur.execute(
                    "SELECT Titel FROM Veranstaltung WHERE ID =%s", [blatt['Veranstaltung']])
                veranstaltung_fetch = cur.fetchone()
                form.VeranstaltungTitel = veranstaltung_fetch["Titel"]

                # Close connection
                cur.close()

                return render_template('dashboard/edit_blatt.html', veranSt=veranSt)
        else:
            # Close connection
            cur.close()

            flash("Permission Denied, You are not the owner of this lecture", "danger")
            return redirect(url_for("dashboard_views.dashboard"))
    return redirect(url_for('dashboard_views.dashboard'))


@ dashboard_views.route("/blatt/blatt_aufgaben/<blatt_id>")
@ is_logged_in
def blatt_aufgaben(blatt_id):

    # Create cursor
    cur = mysql.connection.cursor()
    # Get aufgaben
    result = cur.execute(
        "SELECT Aufgaben.Name, helpers.aufgaben_id, helpers.point_a FROM Aufgaben INNER JOIN helpers ON Aufgaben.ID = helpers.aufgaben_id  WHERE helpers.blatt_id=%s ORDER BY helpers.order_a ASC ", [blatt_id])

    blatt_aufgaben = cur.fetchall()

    # Close connection
    cur.close()

    if result > 0:
        response = make_response(jsonify(blatt_aufgaben), 200)
        return response


# Delete Blatt
@ dashboard_views.route('/dashboard/veranstaltungs/blatt/delete/<string:ID>/<int:veranSt>', methods=['POST'])
@ is_logged_in
def delete_blatt(ID, veranSt):
    # Create cursor
    cur = mysql.connection.cursor()
    # Check if user is owner of the blatt
    result = cur.execute("SELECT * FROM dashboard  WHERE user_id=%s AND veranSt_id=%s", [
        session["id"], veranSt])

    user = cur.fetchone()
    if result > 0:

        # Execute
        cur.execute("DELETE FROM Blatt WHERE ID = %s", [ID])

        # Commit to DB
        mysql.connection.commit()

        # Close connection
        cur.close()

        flash('Blatt Deleted', 'success')

        return redirect(url_for('dashboard_views.blatt', id=veranSt))
    else:
        flash("Permission Denied, You are not the owner of this Blatt", "danger")

    # Close connection
    cur.close()

    return redirect(url_for('dashboard_views.blatt', id=veranSt))


# Delete Batt Aufgaben
@ dashboard_views.route("/edit_blatt/delete_aufgaben/<int:aufgaben_id>", methods=["DELETE"])
@ is_logged_in
def delete_blatt_aufgaben(aufgaben_id):
    # Create cursor
    cur = mysql.connection.cursor()

    # Execute
    cur.execute("DELETE FROM helpers WHERE aufgaben_id = %s", [aufgaben_id])

    # Commit to DB
    mysql.connection.commit()

    # Close connection
    cur.close()

    res = make_response(jsonify({}), 204)
    return res

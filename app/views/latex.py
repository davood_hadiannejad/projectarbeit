
from flask import render_template, request, flash, redirect, url_for, logging, session, send_file, send_from_directory, safe_join, abort
from wtforms import BooleanField, Form, IntegerField, PasswordField, StringField, TextAreaField, validators, widgets
import os
import re
from flask.blueprints import Blueprint
from flask import current_app
from flask.json import jsonify
from flask.helpers import make_response


#  latex's libraries
import pylatex as pl
from pylatex import Document, Section, Subsection, Command, PageStyle, Head, Foot, MiniPage, LargeText, MediumText, LineBreak, MultiColumn, TextColor,  simple_page_number, Tabularx
from pylatex.utils import italic, NoEscape, escape_latex, bold
from pylatex.base_classes import Environment
from pylatex.package import Package
from pylatex.basic import NewLine

# import the function if an user logged in
from app.views.admin_views import is_logged_in
#  import mysql from our app file
from app import mysql
from pylatex.figure import Figure, StandAloneGraphic
import sys
from pylatex.table import Tabu
import glob


latex = Blueprint('latex', __name__, static_folder="static",
                  template_folder="templates")


class LatexDocument():
    def __init__(self, doc, blatt_data, verstaltung):
        super().__init__()
        self.doc = doc
        self.blatt_data = blatt_data
        self.verstaltung = verstaltung

    # Header   # nopep8
    def generate_header(self):

        # Set up header and footer
        header = PageStyle("header")
        header.append(NoEscape(r"\renewcommand{\footrulewidth}{0.4pt}"))

        # Creat left Footer
        # with header.create(Foot("L")):
        #     header.append("Left Footer")

        # Create right Footer
        with header.create(Foot("R")):
            header.append(
                NoEscape(r"\Page \thepage"))

        # apend the header to document
        self.doc.preamble.append(header)
        self.doc.change_document_style("header")

        # left header
        with self.doc.create(MiniPage(width=NoEscape(r"0.49\textwidth"), pos="h", align="l")):
            # self.doc.append(NoEscape(r"\flushleft"))
            self.doc.append(NoEscape(self.verstaltung["Verantwortlich"]))

        # fill the space between the minipages
        self.doc.append(NoEscape(r"\hfill"))

        # right header
        with self.doc.create(MiniPage(width=NoEscape(r"0.49\textwidth"), pos='t!', align='r')):
            # self.doc.append(NoEscape(r"\flushright"))
            self.doc.append(NoEscape(self.verstaltung["Mitarbeiter"]))

        # space between the header and title
        self.doc.append(NoEscape(r'\vspace{0.4cm}'))
        self.doc.append(LineBreak())
        self.doc.append(
            NoEscape(r"\noindent\makebox[\linewidth]{\rule{\textwidth}{0.4pt}}"))
        self.doc.append(NoEscape(r'\vspace{0.4cm}'))
        self.doc.append(LineBreak())

        # Add Title
        with self.doc.create(MiniPage(align='c')):
            self.doc.append(NoEscape(self.verstaltung["Titel"]))
            self.doc.append(NoEscape(r'\vspace{0.2cm}'))
            self.doc.append(LineBreak())
            self.doc.append(
                LargeText(bold(NoEscape(f"Aufgabenblatt {self.blatt_data[0]['Nr']}: {self.blatt_data[0]['Blatt.Name']}"))))
            self.doc.append(NoEscape(r'\vspace{0.4cm}'))

        # Some space and a vertical line
        self.doc.append(LineBreak())
        self.doc.append(NoEscape(r'\vspace{0.2cm}'))
        self.doc.append(
            NoEscape(r"\noindent\makebox[\linewidth]{\rule{\textwidth}{0.4pt}}"))
        self.doc.append(LineBreak())

        # Mini Page for Ausgabe
        with self.doc.create(MiniPage(width=NoEscape(
                r"0.49\textwidth"), pos='h', align="l")):
            self.doc.append("Ausgabe: ")
            self.doc.append(NoEscape(self.blatt_data[0]["Ausgabe"]))

        # fill the space between the minipages
        self.doc.append(NoEscape(r"\hfill"))

        # Mini Page for Abgabe
        with self.doc.create(MiniPage(width=NoEscape(r"0.49\textwidth"), pos='t!',
                                      align='r')):
            self.doc.append("Abgabe: ")
            self.doc.append(NoEscape(self.blatt_data[0]["Abgabe"]))

        # Space between the above mini pages and the small below text
        self.doc.append(NoEscape(r'\vspace{0.2cm}'))
        self.doc.append(LineBreak())

        with self.doc.create(MiniPage(pos='c', align="c")):
            self.doc.append(MediumText(NoEscape(self.verstaltung["Kopftext"])))

        self.doc.append(NoEscape(r'\vspace{0.2cm}'))

        # Line break to and some space to separae above mini pages from main text
        self.doc.append(LineBreak())
        self.doc.append(
            NoEscape(r"\noindent\makebox[\linewidth]{\rule{\textwidth}{0.4pt}}"))
        self.doc.append(LineBreak())
        self.doc.append(NoEscape(r'\vspace{0.2cm}'))
        self.doc.append(LineBreak())

    def fill_document(self, loesung):

        for index, val in enumerate(self.blatt_data):
            with self.doc.create(MiniPage(width=NoEscape(
                    r"0.49\textwidth"), pos='h', align="l")):
                self.doc.append(
                    LargeText(bold(NoEscape(f"Aufgabe {index + 1 }   "))))
                self.doc.append(bold(NoEscape(val["Name"])))

            # fill the space between the minipages
            self.doc.append(NoEscape(r"\hfill"))

            with self.doc.create(MiniPage(width=NoEscape(r"0.49\textwidth"), pos='t!',
                                          align='r')):
                self.doc.append("(")
                self.doc.append(NoEscape(val["point_a"]))
                self.doc.append(NoEscape("\hspace{0.1cm} "))
                self.doc.append(NoEscape("Punkte)"))

            # Space between the above mini pages and the small below text
            self.doc.append(NoEscape(r'\vspace{0.5cm}'))
            self.doc.append(LineBreak())

            self.doc.append(NoEscape(val["Aufgabe"]))
            if loesung == 1:
                self.doc.append(NoEscape(val["Loesung"]))
            # self.doc.append(NoEscape(r"\indentfirst\newcommand\measurepage{\the\dimexpr\pagegoal-\pagetotal-\baselineskip\relax}"))
            self.doc.append(NoEscape(r"\par"))
            # self.doc.append(NewLine())
            self.doc.append(NoEscape(r'\vspace{1cm}'))

        # Space between above conntent
        self.doc.append(NoEscape(r'\vspace{2cm}'))
        self.doc.append(LineBreak())
        with self.doc.create(MiniPage(align='c')):
            self.doc.append(
                LargeText(bold(NoEscape(self.verstaltung["Fusstext"]))))


@ latex.route('/dashboard/veranstaltungs/blatt/print/<int:veranSt>/<string:ID>/<int:loesung>', methods=['GET', 'POST'])
@ is_logged_in
def create_blatt(veranSt, ID, loesung):

    # Create cursor
    cur = mysql.connection.cursor()

    result = cur.execute("SELECT * FROM dashboard  WHERE user_id=%s AND veranSt_id=%s", [
        session["id"], veranSt])

    user = cur.fetchone()

    if result > 0:

        # Get Blatt by ID
        result = cur.execute(
            "SELECT  helpers.point_a, Aufgaben.Name,  Aufgaben.Aufgabe,  Aufgaben.Loesung, Blatt.Name,  Blatt.Abgabe, Blatt.Ausgabe,  Blatt.Nr, Blatt. Veranstaltung, Blatt.Zusatz FROM helpers INNER JOIN Aufgaben ON Aufgaben.ID = helpers.aufgaben_id INNER JOIN Blatt ON Blatt.ID = helpers.blatt_id WHERE helpers.blatt_id =%s ORDER BY helpers.order_a ASC ", [ID])

        blatt_data = cur.fetchall()

        if result > 0:
            # Get Veranstaltung
            veran_result = verstaltung = cur.execute(
                "SELECT * FROM Veranstaltung WHERE ID= %s", [blatt_data[0]["Veranstaltung"]])

            verstaltung = cur.fetchone()

            # Close connection
            cur.close()

            # Document Geometry
            geometry_options = {
                "head": "0pt",
                "tmargin": "10mm",
                "lmargin": "25mm",
                "rmargin": "20mm",
                "bmargin": "20mm",
                "includeheadfoot": True,
                "landscape": False
            }

            # Create Document
            # See https://jeltef.github.io/PyLaTeX/current/pylatex/pylatex.document.html
            doc = pl.Document(default_filepath='default_filepath', documentclass='article', fontenc="T1", inputenc="utf8", lmodern=True,
                              data=None, page_numbers=True, indent=False, document_options=["a4paper", "12pt"], geometry_options=geometry_options)

            # Packages
            doc.packages.append(Package('graphicx'))
            # doc.packages.append(Package('epstopdf'))
            doc.packages.append(Package("indentfirst"))
            doc.append(NoEscape(r"\graphicspath{{./img/}}"))
            doc.preamble.append(Command('selectlanguage', 'German, English'))
            # doc.preamble.append(Command($PATH, 'German, English'))

            #  Initialize the Latex Class
            latexDoc = LatexDocument(doc, blatt_data, verstaltung)

            # Call Fucntion to had header, footer, ...
            latexDoc.generate_header()

            #  Call funtion to add Aufgaben and loesungen
            latexDoc.fill_document(loesung)

            # Create a directory for latex outputs
            dir = "texfiles"
            #  current  path
            cur_path = os.getcwd()

            if not os.path.isdir(dir):
                os.makedirs(dir)

            # change path to the created folder
            os.chdir(cur_path + "/" + dir)

            # clear the existed file in the folder
            files = glob.glob(os.getcwd() + "/*.*", recursive=True)
            for file in files:
                os.remove(file)

            # See https://jeltef.github.io/PyLaTeX/current/pylatex/pylatex.document.html#pylatex.document.Document.generate_pdf
            try:
                os.path.join(dir, doc.generate_pdf(
                    blatt_data[0]["Blatt.Name"], clean=True, clean_tex=True, compiler='pdflatex', compiler_args=None))
            except Exception as e:
                tex = doc.dumps()  # The document as string in LaTeX syntax

            # CD to original working directory
            os.chdir(cur_path)

            try:
                return send_file(cur_path + "/" + dir + "/" + blatt_data[0]["Blatt.Name"]+".pdf")
            except Exception as e:
                return str(e)
        else:
            # Close connection
            cur.close()
            flash("Blatt Not Found", "danger")
            return redirect(url_for("dashboard_views.dashboard"))

    else:
        # Close connection
        cur.close()
        flash("Permission Denied, You are not the owner of this lecture", "danger")
        return redirect(url_for("dashboard_views.dashboard"))

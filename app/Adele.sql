-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 20, 2020 at 01:03 PM
-- Server version: 8.0.20-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Adele`
--

-- --------------------------------------------------------

--
-- Table structure for table `Aufgaben`
--

CREATE TABLE users(
id bigint AUTO_INCREMENT,
   first_name VARCHAR(100),
   last_name VARCHAR(100),
   email VARCHAR(100),
   password VARCHAR(250),
   dept VARCHAR(100),
   is_admin TINYINT(1) DEFAULT 0,
   register_date DATETIME DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY(id)
);
-- --------------------------------------------------------

--
-- Table structure for table `Aufgaben`
--

CREATE TABLE `Aufgaben` (
  `ID` bigint NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `REFID` bigint NOT NULL DEFAULT '0',
  `Name` varchar(105) NOT NULL DEFAULT '',
  `Titel` varchar(150) NOT NULL DEFAULT '',
  `Aufgabe` mediumtext NOT NULL,
  `Loesung` mediumtext NOT NULL,
  `User` bigint NOT NULL DEFAULT '0',
  `Gebiet` varchar(40) NOT NULL DEFAULT '',
  `version` bigint NOT NULL DEFAULT '0',
  `Rechte` int NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Aufgaben`
--

INSERT INTO `Aufgaben` (`ID`, `ts`, `REFID`, `Name`, `Titel`, `Aufgabe`, `Loesung`, `User`, `Gebiet`, `version`, `Rechte`) VALUES
(6500, '2011-12-13 11:04:22', 6443, 'Flughafen', 'Flughafen', 'Ein kleiner Flughafen möchte ein Programm zur Anzeige ein- und abgehender Flüge umsetzen. Zur Repräsentation eines Flugs wurde schon eine Klasse \\texttt{Flight} entworfen:\r\n\\begin{verbatim}\r\nclass Flight {\r\n    int flightNumber;    // Flugnummer\r\n    String location;     // Abflugs-/Zielort\r\n    String gate;         // Gate\r\n    String time;         // Abflugs-/Ankunftszeit\r\n    boolean inOut;       // ein- oder abgehender Flug\r\n}\r\n\\end{verbatim}\r\nNun fehlt noch die Umsetzung der Klassen zur Anzeige und Verwaltung der Flüge: Setzen Sie eine Klasse \\texttt{Airport} in der Programmiersprache Java um. Diese soll eine beschränkte Anzahl von ein- und abgehenden Flügen mit Hilfe einer Reihung verwalten. Folgende Methoden sollen zu diesem Zweck für die Klasse \\texttt{Airport} implementiert werden:\r\n\\begin{itemize}\r\n\\item \\begin{verbatim}Airport(int maxFlights)\\end{verbatim}\r\nKonstruktor: Initialisiert ein Objekt der Klasse \\texttt{Airport}, dieses kann maximal \\texttt{maxFlights} eingehende und abgehende Flüge verwalten;\r\n\\item \\begin{verbatim}void addNewFlight(Flight flight)\\end{verbatim}\r\nMethode die einen Flug hinzufügt, existiert schon ein ein- oder abgehender Flug mit der selben Flugnummer soll eine Fehlermeldung ausgegeben werden, eine Fehlermeldung soll auch dann ausgegeben werden falls die maximale Zahl von Flügen überschritten wird;\r\n\\item \\begin{verbatim}void removeFlight(int flightNumber)\\end{verbatim}\r\nMethode die den ein- oder abgehenden Flug mit der Flugnummer \\texttt{flightNumber} wieder entfernt, existiert kein Flug mit der entsprechenden Flugnummer soll nichts passieren;\r\n\\item \\begin{verbatim}listDeparturesOnScreen()\\end{verbatim}\r\nMethode die alle abgehenden Flüge (mit Flugnummer, Abflugszeit, Gate und Zielort) ausgibt;\r\n\\item \\begin{verbatim}listArrivalsOnScreen()\\end{verbatim}\r\nMethode die alle eingehenden Flüge (mit Flugnummer, Ankunftszeit, Gate und Abflugsort) ausgibt;\r\n\\end{itemize}', 'beispielsweise:\r\n\\begin{verbatim}\r\nclass Flight {\r\n    int flightNumber; String location, gate, time; boolean inOut;\r\n}\r\n\r\nclass Airport {\r\n\r\n    Flight[] allFlights;\r\n    int pointer;\r\n\r\n    Airport(int max_flights) {\r\n        if (max_flights >= 0) {\r\n            allFlights = new Flight[max_flights];\r\n        } else {\r\n            allFlights = new Flight[0];\r\n        }\r\n        pointer = 0;\r\n    }\r\n\r\n    boolean flightExists(int flightNumber) {\r\n        for (int i = 0; i < pointer; i++) {\r\n            if (allFlights[i].flightNumber == flightNumber) {\r\n                return true;\r\n            }\r\n        }\r\n        return false;\r\n    }\r\n    void addNewFlight(Flight flight) {\r\n        if (flight == null) {\r\n            Out.println(\"ERROR: unspecified flight !\");\r\n        } else if (flightExists(flight.flightNumber)) {\r\n            Out.println(\"ERROR: duplicate flight number !\");\r\n        } else if (pointer >= allFlights.length) {\r\n            Out.println(\"ERROR: max number of flights !\");\r\n        } else {\r\n            allFlights[pointer++] = flight;\r\n        }\r\n    }    \r\n    void removeFlight(int flightNumber) {\r\n        for (int i = 0; i < pointer; i++) {\r\n            if (allFlights[i].flightNumber == flightNumber) {\r\n                allFlights[i] = allFlights[--pointer];\r\n            }\r\n        }\r\n    }\r\n    void listDeparturesOnScreen() {\r\n        Out.println(\"Flight \\t To \\t Time \\t Gate\");\r\n        for (int i = 0; i < pointer; i++) {\r\n            if (!allFlights[i].inOut) {\r\n                Out.println(allFlights[i].flightNumber + \"\\t\"\r\n                        + allFlights[i].location + \"\\t\"\r\n                        + allFlights[i].time + \"\\t\"\r\n                        + allFlights[i].gate);\r\n            }\r\n        }\r\n    }\r\n    void listArrivalsOnScreen() {\r\n        Out.println(\"Flight \\t From \\t Time \\t Gate\");\r\n        for (int i = 0; i < pointer; i++) {\r\n            if (allFlights[i].inOut) {\r\n                Out.println(allFlights[i].flightNumber + \"\\t\"\r\n                        + allFlights[i].location + \"\\t\"\r\n                        + allFlights[i].time + \"\\t\"\r\n                        + allFlights[i].gate);\r\n            }\r\n        }\r\n    }\r\n\r\n}\r\n\\end{verbatim}', 1, 'Flughafen, Klassen, Java, GMP, WS 2011', 0, 3),
(8843, '2014-04-22 15:53:27', 8830, 'Klassendiagramm (Räume)', 'Klassendiagramm', 'Erstellen Sie für die folgende Beschreibung ein Klassendiagramm:\r\n\r\nDie Räume einer Universität sollen durch ein Programm verwaltet werden. Dabei hat ein Raum eine Adresse sowie eine Anzahl von Sitzplätzen und ist beliebig vielen Lehrveranstaltungen zugeordnet. Eine Lehrveranstaltung besitzt einen Namen sowie eine Teilnehmeranzahl und ist maximal einem Raum zugeordnet. Man kann für eine Lehrveranstaltung einen Raum reservieren oder wieder freigeben.\r\n\r\nEine Lehrveranstaltung ist entweder eine Blockveranstaltung oder eine Turnusveranstaltung. Einer Blockveranstaltung ist ein Datum zugeordnet, einer Turnusveranstaltung hingegen ein Wochentag. Der Typ der Veranstaltung muss bei der Raumreservierung und -freigabe berücksichtigt werden.', 'Ein mögliches Klassendiagramm siehe unten. Lehrveranstaltung kann/sollte eine abstrakte Klasse sein, da die beiden Methoden überschrieben werden müssen. Block- und Turnusveranstaltung sind Unterklassen. Einem Raum sind 0 bis beliebig viele Lehrveranstaltungen zugeordnet. Eine Lehrveranstaltung hat keinen oder genau einen Raum. Die Raute ist nicht so wichtig (auch Assoziation möglich).\r\n\\begin{center}\r\n\\includegraphics[width=.7\\textwidth]{diagramm}\r\n\\end{center}\r\n', 1, 'Klassendiagramm, OOP, SS 2014', 0, 3),
(8873, '2014-05-06 15:30:01', 8862, 'Klassendiagramm Uni2', 'Raumverwaltung', 'In der Entwurfsphase wird das Klassendiagramm der Analysephase verfeinert und der gewählten Implementierungssprache angepasst. Im Folgenden ist das derart verfeinerte Klassendiagramm für die Raumverwaltung einer Hochschule gegeben, die mit der Programmiersprache Java umgesetzt werden soll:\r\n\r\n\\begin{center}\r\n\\includegraphics[width=.4\\textwidth]{kd_uni_norm}\r\n\\end{center}\r\n\r\n\\begin{enumerate}\r\n\\item[a)] Erweitern Sie das Klassendiagramm nun noch um die beiden Klassen \\verb|SeminarRaum| und \\verb|Labor|. Diese beiden Klassen sind aufgebaut wie die Klasse \\verb|Hörsaal|. Anstelle von Vorlesungen sind einem Seminarraum jedoch Übungen und einem Labor Praktika zugeordnet. Passen Sie die Konstruktoren, Methoden und Variablen entsprechend an.\r\n\r\n\\emph{Anmerkung: Der Einfachheit halber gehen wir davon aus, dass alle Hörsäle, Seminarräume und Labore unterschiedliche Adressen besitzen.}\r\n\r\n\\item[b)] Geben Sie für das erweiterte Klassendiagramm aus der Teilaufgabe a) eine Implementierung in Java an. Neben der Klassenstruktur sollen auch die Variablen und Methoden umgesetzt werden.\r\n\\end{enumerate}\r\n\\emph{Hinweis: Die Klassen \\texttt{Übung}, \\texttt{Praktikum} und \\texttt{Vorlesung} müssen nicht modelliert werden. Für die Implementierung können Sie diese Klassen als gegeben ansehen.}', 'a)\r\n\\begin{itemize}\r\n\\item \\texttt{SeminarRaum} und \\texttt{Labor} kommen neu hinzu mit jeweils speziellem Feld (siehe etwa unten)\r\n\\item wer eine weitere Oberklasse \\texttt{Raum} eingeführt und die Vererbung verwendet hat ist ok (sogar sauberer)\r\n\\end{itemize}\r\n\\begin{center}\r\n\\includegraphics[width=.99\\textwidth]{kd_uni_alles}\r\n\\end{center}\r\nb)\r\n\\begin{verbatim}\r\nclass SeminarRaum {\r\n    Adresse adresse; Integer plätze; Übung[] übungen;\r\n    SeminarRaum(Integer plz, Adresse adr, Übung[] übg) {\r\n        plätze = plz; adresse = adr; übungen = übg;\r\n    }\r\n    Integer getPlätze() { return plätze; }\r\n    Adresse getAdresse() { return adresse; }\r\n    Übung[] getÜbungen() { return übungen; }\r\n}\r\nclass Hörsaal {\r\n    Adresse adresse; Integer plätze; Vorlesung[] vorlesungen;\r\n    Hörsaal(Integer plz, Adresse adr, Vorlesung[] vlg) {\r\n        plätze = plz; adresse = adr; vorlesungen = vlg;\r\n    }\r\n    Integer getPlätze() { return plätze; }\r\n    Adresse getAdresse() { return adresse; }\r\n    Vorlesung[] getVorlesungen() { return vorlesungen; }\r\n}\r\nclass Labor {\r\n    Adresse adresse; Integer plätze; Praktikum[] praktika;\r\n    Labor(Integer plz, Adresse adr, Praktikum[] prk) {\r\n        plätze = plz; adresse = adr; praktika = prk;\r\n    }\r\n    Integer getPlätze() { return plätze; }\r\n    Adresse getAdresse() { return adresse; }\r\n    Praktikum[] getPraktika() { return praktika; }\r\n}\r\nclass Adresse {\r\n    String plz, straße, ort;\r\n    Adresse(String straße, String plz, String ort) {\r\n        this.straße = straße; this.plz = plz; this.ort = ort;\r\n    }\r\n    String getPLZ() { return plz; }\r\n    String getStraße() { return straße; }\r\n    String getOrt() { return ort; }\r\n}\r\n\\end{verbatim}', 1, 'Klassendiagramm, OOP, SS 2013', 0, 3),
(10887, '2017-04-21 09:17:34', 5632, 'Kopie: Klassenimplementierung', 'Implementierung', 'In der Vorlesung wurde Ihnen für die Vereinzelungseinheit ein Klassendiagramm präsentiert (siehe unten). Erstellen Sie für dieses Klassendiagramm eine Implementierung (das Grundgerüst aus den Klassen) !\r\n\r\nAchten Sie dabei insbesondere darauf, dass in der Implementierung keine Information aus dem Klassendiagramm verloren geht.\r\n\r\n\\emph{Hinweis: Sie können dazu die Pseudosprache aus der Vorlesung oder Java verwenden. Der Inhalt der Methoden kann vernachlässigt werden, ebenso wie die genaue Spezifizierung der Variablen.}\r\n\r\n\\begin{center}\r\n\\includegraphics[width=.9\\textwidth]{vediag}\r\n\\end{center}\r\n', 'Wichtig ist hier die Klassenstruktur (Vererbungsbeziehung) sowie die Teil-Ganzes-Beziehungen (Variablen für die einzelnen Teile, vermutlich Arrays oder einzelne Variablen). Die Namen der Klassen und Methoden müssen mit dem Klassendiagramm übereinstimmen.\r\n\r\n\\begin{minipage}[t]{.9\\textwidth}\r\n\\begin{verbatim}\r\nclass Ball\r\nvar\r\n    einheit : Vereinzelungseinheit;\r\n\\end{verbatim}\r\n\\end{minipage}\r\n\r\n\\vspace{1cm}\r\n\r\n\\begin{minipage}[t]{.49\\textwidth}\r\n\\begin{verbatim}\r\nclass Gerät\r\nvar\r\n    ea : EinAus;\r\n    n  : Seriennummer;\r\nmeth\r\n    Gerät.ein()\r\n    begin\r\n    ...\r\n    end.Gerät.ein\r\n\r\n    Gerät.aus()\r\n    begin\r\n    ...\r\n    end.Gerät.aus\r\n\r\n    Gerät.get_Seriennummer()\r\n    begin\r\n    ...\r\n    end.Gerät.get_Seriennummer\r\n\\end{verbatim}\r\n\\end{minipage}\r\n\\begin{minipage}[t]{.49\\textwidth}\r\n\\begin{verbatim}\r\nclass Schieber extends Gerät\r\nvar\r\n    s       : Stellung;\r\n    einheit : Vereinzelungseinheit;\r\nmeth\r\n    Schieber.öffnen()\r\n    begin\r\n    ...\r\n    end.Schieber.öffnen\r\n\r\n    Schieber.schließen()\r\n    begin\r\n    ...\r\n    end.Schieber.schließen\r\n\\end{verbatim}\r\n\\end{minipage}\r\n\r\n\\vspace{1cm}\r\n\r\n\\begin{minipage}[t]{.49\\textwidth}\r\n\\begin{verbatim}\r\nclass Sensor extends Gerät\r\nvar\r\n    z       : Zustand;\r\n    einheit : Vereinzelungseinheit;\r\nmeth\r\n    Sensor.ist_aktiv()\r\n    begin\r\n    ...\r\n    end.Sensor.ist_aktiv\r\n\r\n    Sensor.ist_inaktiv()\r\n    begin\r\n    ...\r\n    end.Sensor.ist_inaktiv\r\n\\end{verbatim}\r\n\\end{minipage}\r\n\\begin{minipage}[t]{.49\\textwidth}\r\n\\begin{verbatim}\r\nclass Ventil extends Gerät\r\nvar\r\n    s       : Stellung;\r\n    einheit : Vereinzelungseinheit;\r\nmeth\r\n    Ventil.öffnen()\r\n    begin\r\n    ...\r\n    end.Ventil.öffnen\r\n\r\n    Ventil.schließen()\r\n    begin\r\n    ...\r\n    end.Ventil.schließen\r\n\\end{verbatim}\r\n\\end{minipage}\r\n\r\n\\vspace{1cm}\r\n\r\n\\begin{minipage}[t]{.9\\textwidth}\r\n\\begin{verbatim}\r\nclass Vereinzelungseinheit extends Gerät\r\nvar\r\n    bälle    : array[1..7] of Ball;\r\n    schieber : array[1..2] of Schieber;\r\n    sensoren : array[1..2] of Sensor;\r\n    ventil   : Ventil;\r\nmeth\r\n    Vereinzelungseinheit.zuführen_Ball()\r\n    begin\r\n    ...\r\n    end.Vereinzelungseinheit.zuführen_Ball\r\n\r\n    Vereinzelungseinheit.freigeben_Ball()\r\n    begin\r\n    ...\r\n    end.Vereinzelungseinheit.freigeben_Ball\r\n\r\n    Vereinzelungseinheit.wartungszyklus()\r\n    begin\r\n    ...\r\n    end.Vereinzelungseinheit.wartungszyklus\r\n\\end{verbatim}\r\n\\end{minipage}', 1, 'Implementierung Klasse, OOP, SS 2011', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `Blatt`
--

CREATE TABLE `Blatt` (
  `ID` bigint NOT NULL,
  `Nr` tinyint NOT NULL DEFAULT '0',
  `Name` varchar(100) NOT NULL DEFAULT '',
  `Titel` mediumtext,
  `Veranstaltung` bigint NOT NULL DEFAULT '0',
  `Ausgabe` varchar(35) DEFAULT NULL,
  `Abgabe` varchar(35) DEFAULT NULL,
  `Zusatz` mediumtext,
  `Stil` bigint NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Blatt`
--

INSERT INTO `Blatt` (`ID`, `Nr`, `Name`, `Titel`, `Veranstaltung`, `Ausgabe`, `Abgabe`, `Zusatz`, `Stil`) VALUES
(828, 3, 'Klassendiagramme', 'Klassendiagramme', 158, '10. Mai 2018', '17. Mai 2018', '', 0),
(830, 3, 'Klassen in Java', 'Klassen in Java', 158, '29. Mai 2018', '05. Jun 2018', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Makro`
--

CREATE TABLE `Makro` (
  `ID` bigint NOT NULL,
  `VID` bigint NOT NULL DEFAULT '0',
  `BID` bigint NOT NULL DEFAULT '0',
  `BAID` bigint NOT NULL DEFAULT '0',
  `global` tinyint NOT NULL DEFAULT '0',
  `Key` varchar(30) NOT NULL DEFAULT '',
  `Value` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Makro`
--

INSERT INTO `Makro` (`ID`, `VID`, `BID`, `BAID`, `global`, `Key`, `Value`) VALUES
(1, 0, 0, 0, 1, 'noo', 'Wolfgang Ortmann'),
(9, 0, 0, 620, 0, 'test', 'Dies ist ein Test'),
(4, 0, 0, 0, 1, 'CStringBasics', 'Aus der Klasse {\\bf string} dürfen hier nur die Operatoren (=, +, [], $<$, ==, ...) und die Methoden {\\bf length() bzw. size()} verwendet werden. C-Funktionen für Zeichen ({\\bf char}) dürfen nicht verwendet werden.'),
(10, 27, 0, 0, 0, 'Language', 'Simplicius'),
(8, 0, 0, 617, 0, 'Language', 'C++'),
(11, 0, 154, 0, 0, 'Language', 'C++'),
(13, 0, 0, 649, 0, 'Parameter', 'Parameterwert 1'),
(14, 0, 0, 650, 0, 'Parameter', 'Parameterwert 2'),
(15, 0, 0, 659, 0, 'Parameter', '11'),
(16, 0, 0, 669, 0, 'Parameter', 'TEST\\_PARAMETER'),
(17, 0, 0, 672, 0, 'Parameter', '\'Dies ist der Parameter\''),
(58, 38, 0, 0, 0, 'Language', 'C++'),
(57, 39, 0, 0, 0, 'Language', '{f C}'),
(21, 28, 0, 0, 0, 'Veranstaltung', 'ADELEDEMO'),
(52, 0, 0, 821, 0, 'Language', 'C++'),
(53, 0, 178, 0, 0, 'Language', 'Java'),
(55, 0, 0, 889, 0, 'Language', 'C++'),
(56, 28, 0, 0, 0, 'Language', '$C^2$'),
(44, 0, 162, 0, 0, 'Ausdruck1', '(a+b)*(c-d)+4*-X'),
(45, 0, 162, 0, 0, 'Ausdruck2', '(x*y + 10 *a )/ 5 * z'),
(46, 0, 164, 0, 0, 'Ausdruck2', '(a+b)*(c-d)+4*-k'),
(47, 0, 164, 0, 0, 'Ausdruck1', '(x*y + 10 *a )/ 5 * z'),
(48, 0, 164, 0, 0, 'Ausdruck3', 'a * (x - b) * (x - b) + c\r\n'),
(49, 0, 164, 0, 0, 'Ausdruck4', '(4+7*a-b)/((4+7)*(a-b))'),
(50, 0, 0, 0, 1, 'Blatt', 'Aufgabenblatt'),
(51, 0, 0, 0, 1, 'Loesung', 'Lösung zu Blatt'),
(59, 0, 207, 0, 0, 'Language', 'C++'),
(60, 42, 0, 0, 0, 'Language', 'C++'),
(61, 0, 208, 0, 0, 'Language', 'C++'),
(62, 0, 211, 0, 0, 'Language', 'C++'),
(64, 48, 0, 0, 0, 'Language', 'C++'),
(65, 0, 246, 0, 0, 'Language', 'C++'),
(66, 53, 0, 0, 0, 'Language', 'C++'),
(72, 57, 0, 0, 0, 'Language', 'C++'),
(74, 0, 303, 0, 0, 'Language', 'C++'),
(73, 56, 0, 0, 0, 'Language', 'C++'),
(75, 61, 0, 0, 0, 'Language', 'C++'),
(76, 64, 0, 0, 0, 'Language', 'C++'),
(77, 68, 0, 0, 0, 'Language', 'C++'),
(78, 0, 0, 1588, 0, '', ''),
(79, 75, 0, 0, 0, 'Language', 'C++'),
(80, 77, 0, 0, 0, 'Language', 'C++'),
(81, 81, 0, 0, 0, 'Language', 'C++'),
(82, 0, 406, 0, 0, 'Language', 'C++'),
(83, 0, 0, 1810, 0, 'int1', 'isieben'),
(84, 0, 0, 1810, 0, 'val1', '7'),
(85, 0, 0, 1810, 0, 'int2', 'ivier'),
(86, 0, 0, 1810, 0, 'val2', '4'),
(87, 0, 0, 1810, 0, 'double', 'dsechs'),
(88, 0, 0, 1810, 0, 'val3', '6.0'),
(89, 0, 406, 0, 0, 'int1', 'ineun'),
(90, 0, 406, 0, 0, 'val1', '9'),
(91, 0, 406, 0, 0, 'int2', 'ivier'),
(92, 0, 406, 0, 0, 'val2', '4'),
(93, 0, 406, 0, 0, 'double', 'dsechs'),
(94, 0, 406, 0, 0, 'val3', '6.0'),
(95, 88, 0, 0, 0, 'Language', 'C'),
(96, 85, 0, 0, 0, 'Language', 'C++'),
(97, 94, 0, 0, 0, 'Language', 'C++'),
(98, 95, 0, 0, 0, 'Language', 'C++'),
(99, 0, 487, 0, 0, 'int1', 'vier'),
(100, 0, 487, 0, 0, 'int2', 'sieben'),
(101, 0, 487, 0, 0, 'val1', '4'),
(102, 0, 487, 0, 0, 'val2', '7'),
(103, 0, 487, 0, 0, 'double', 'sechs'),
(104, 0, 487, 0, 0, 'val3', '6.0'),
(105, 102, 0, 0, 0, 'Language', 'C++'),
(106, 0, 0, 2220, 0, 'int1', 'i1'),
(107, 0, 0, 2220, 0, 'int2', 'i2'),
(108, 0, 0, 2220, 0, 'double', 'd1'),
(109, 0, 0, 2220, 0, 'val1', '9'),
(110, 0, 0, 2220, 0, 'val2', '5'),
(111, 0, 0, 2220, 0, 'val3', '3.14159265'),
(112, 105, 0, 0, 0, 'Language', 'C++'),
(113, 0, 0, 2273, 0, 'int1', 'i_sieben'),
(114, 0, 0, 2273, 0, 'val1', '7'),
(115, 0, 0, 2273, 0, 'int2', 'i_vier'),
(116, 0, 0, 2273, 0, 'val2', '4'),
(117, 0, 0, 2273, 0, 'double', 'd_sechs'),
(118, 0, 0, 2273, 0, 'val3', '6.0'),
(119, 108, 0, 0, 0, '', ''),
(120, 109, 0, 0, 0, 'Language', 'C++'),
(121, 0, 538, 0, 0, 'Language', 'C'),
(122, 0, 533, 0, 0, 'Language', 'C++'),
(123, 0, 556, 0, 0, 'int1', 'i7'),
(124, 0, 556, 0, 0, 'val1', '7'),
(125, 0, 556, 0, 0, 'int2', 'i4'),
(126, 0, 556, 0, 0, 'val2', '4'),
(127, 0, 556, 0, 0, 'double', 'd6'),
(128, 0, 556, 0, 0, 'val3', '6.283'),
(129, 112, 0, 0, 0, 'Language', 'C++'),
(130, 0, 574, 0, 0, 'int1', 'iNeun'),
(131, 0, 574, 0, 0, 'val1', '9'),
(132, 0, 574, 0, 0, 'int2', 'iDrei'),
(133, 0, 574, 0, 0, 'val2', '3'),
(134, 0, 574, 0, 0, 'val3', '7.0'),
(135, 0, 574, 0, 0, 'double', 'dSieben'),
(136, 116, 0, 0, 0, 'Language', 'C++'),
(137, 0, 0, 2531, 0, 'int1', 'sieben'),
(138, 0, 0, 2531, 0, 'int2', 'drei'),
(139, 0, 0, 2531, 0, 'double', 'pi'),
(140, 0, 0, 2531, 0, 'val1', '7'),
(141, 0, 0, 2531, 0, 'val2', '3'),
(142, 0, 0, 2531, 0, 'val3', '3.45159265'),
(143, 120, 0, 0, 0, 'Language', 'C++'),
(147, 121, 0, 0, 0, 'Language', 'C++'),
(148, 122, 0, 0, 0, 'Language', 'C++'),
(149, 0, 631, 0, 0, 'int1', 'iSieben'),
(150, 0, 631, 0, 0, 'int2', 'iDrei'),
(151, 0, 631, 0, 0, 'val1', '7'),
(152, 0, 631, 0, 0, 'val2', '3'),
(153, 0, 631, 0, 0, 'double', 'dSechs'),
(154, 0, 631, 0, 0, 'val3', '6.0'),
(155, 0, 632, 0, 0, 'int1', 'iVier'),
(156, 0, 632, 0, 0, 'int2', 'iSieben'),
(157, 0, 632, 0, 0, 'double', 'dSechs'),
(158, 0, 632, 0, 0, 'val1', '4'),
(159, 0, 632, 0, 0, 'val2', '7'),
(160, 0, 632, 0, 0, 'val3', '6.0'),
(161, 0, 633, 0, 0, 'int1', 'iNeun'),
(162, 0, 633, 0, 0, 'val1', '9'),
(163, 0, 633, 0, 0, 'int2', 'iZwei'),
(164, 0, 633, 0, 0, 'val2', '2'),
(165, 0, 633, 0, 0, 'double', 'dVierKommaFuenf'),
(166, 0, 633, 0, 0, 'val3', '4.5'),
(167, 126, 0, 0, 0, 'Language', 'C++'),
(168, 0, 660, 0, 0, 'int1', 'iDrei'),
(169, 0, 660, 0, 0, 'int2', 'iNeun'),
(170, 0, 660, 0, 0, 'double', 'dSechs'),
(171, 0, 660, 0, 0, 'val1', '3'),
(172, 0, 660, 0, 0, 'val2', '9'),
(173, 0, 660, 0, 0, 'val3', '6.0'),
(174, 131, 0, 0, 0, 'Language', 'C++'),
(175, 132, 0, 0, 0, 'Language', 'C++'),
(176, 0, 0, 123704, 0, 'int1', 'iAcht'),
(177, 0, 0, 123704, 0, 'int2', 'iZwei'),
(178, 0, 0, 123704, 0, 'double', 'dSechs'),
(179, 0, 0, 123704, 0, 'val1', '8'),
(180, 0, 0, 123704, 0, 'val2', '2'),
(181, 0, 0, 123704, 0, 'val3', '6.0'),
(182, 0, 0, 123697, 0, 'int1', 'iSieben'),
(183, 0, 0, 123697, 0, 'int2', 'iVier'),
(184, 0, 0, 123697, 0, 'double', 'dSechs'),
(185, 0, 0, 123697, 0, 'val1', '7'),
(186, 0, 0, 123697, 0, 'val2', '4'),
(187, 0, 0, 123697, 0, 'val3', '6.0'),
(188, 0, 0, 123709, 0, 'int1', 'iSieben'),
(189, 0, 0, 123709, 0, 'int2', 'iVier'),
(190, 0, 0, 123709, 0, 'double', 'dSechs'),
(191, 0, 0, 123709, 0, 'val1', '7'),
(192, 0, 0, 123709, 0, 'val2', '4'),
(193, 0, 0, 123709, 0, 'val3', '6.0'),
(194, 0, 0, 123709, 0, '', ''),
(195, 0, 706, 0, 0, 'val1', '3.14'),
(196, 140, 0, 0, 0, 'Language', 'C++'),
(197, 0, 0, 123818, 0, '', ''),
(198, 0, 722, 0, 0, '', ''),
(199, 0, 0, 123981, 0, 'double', 'dSechs'),
(200, 0, 0, 123981, 0, 'int1', 'iSieben'),
(201, 0, 0, 123981, 0, 'int2', 'iVier'),
(202, 0, 0, 123981, 0, 'val1', '7'),
(203, 0, 0, 123981, 0, 'val2', '4'),
(204, 0, 0, 123981, 0, 'val3', '6.0'),
(205, 0, 0, 124006, 0, 'double', 'dSix'),
(206, 0, 0, 124006, 0, 'int1', 'iSeven'),
(207, 0, 0, 124006, 0, 'int2', 'iFour'),
(208, 0, 0, 124006, 0, 'val1', '7'),
(209, 0, 0, 124006, 0, 'val2', '4'),
(210, 0, 0, 124006, 0, 'val3', '6.0'),
(211, 0, 0, 124012, 0, 'double', 'dSechs'),
(212, 0, 0, 124012, 0, 'int1', 'iAcht'),
(213, 0, 0, 124012, 0, 'int2', 'iZwei'),
(214, 0, 0, 124012, 0, 'val1', '8'),
(215, 0, 0, 124012, 0, 'val2', '2'),
(216, 0, 0, 124012, 0, 'val3', '6.0'),
(217, 0, 0, 124026, 0, 'double', 'dSix'),
(218, 0, 0, 124026, 0, 'int1', 'iEight'),
(219, 0, 0, 124026, 0, 'int2', 'iTwo'),
(220, 0, 0, 124026, 0, 'val1', '8'),
(221, 0, 0, 124026, 0, 'val2', '2'),
(222, 0, 0, 124026, 0, 'val3', '6.0'),
(224, 0, 0, 124067, 0, '', ''),
(225, 0, 0, 124099, 0, 'double', 'dFuenf'),
(226, 0, 0, 124099, 0, 'int1', 'iNeun'),
(227, 0, 0, 124099, 0, 'int2', 'iDrei'),
(228, 0, 0, 124099, 0, 'val1', '9'),
(229, 0, 0, 124099, 0, 'val2', '3'),
(230, 0, 0, 124099, 0, 'val3', '5.0'),
(231, 0, 0, 124118, 0, 'double', 'dFive'),
(232, 0, 0, 124118, 0, 'int1', 'iNine'),
(233, 0, 0, 124118, 0, 'int2', 'iThree'),
(234, 0, 0, 124118, 0, 'val1', '9'),
(235, 0, 0, 124118, 0, 'val2', '3'),
(236, 0, 0, 124118, 0, 'val3', '5.0'),
(237, 0, 0, 124123, 0, 'double', 'dFuenf'),
(238, 0, 0, 124123, 0, 'int1', 'iSieben'),
(239, 0, 0, 124123, 0, 'int2', 'iVier'),
(240, 0, 0, 124123, 0, 'val1', '7'),
(241, 0, 0, 124123, 0, 'val2', '4'),
(242, 0, 0, 124123, 0, 'val3', '5.0'),
(243, 0, 0, 124129, 0, 'double', 'dFive'),
(244, 0, 0, 124129, 0, 'int1', 'iSeven'),
(245, 0, 0, 124129, 0, 'int2', 'iFour'),
(246, 0, 0, 124129, 0, 'val1', '7'),
(247, 0, 0, 124129, 0, 'val2', '4'),
(248, 0, 0, 124129, 0, 'val3', '5.0'),
(249, 0, 793, 0, 0, 'Language', 'C++'),
(250, 0, 0, 124448, 0, 'double', 'dSechs'),
(251, 0, 0, 124448, 0, 'int1', 'iSieben'),
(252, 0, 0, 124448, 0, 'int2', 'iVier'),
(253, 0, 0, 124448, 0, 'val1', '7'),
(254, 0, 0, 124448, 0, 'val2', '4'),
(255, 0, 0, 124448, 0, 'val3', '6.0'),
(256, 0, 0, 124453, 0, 'double', 'dSechs'),
(257, 0, 0, 124453, 0, 'int1', 'iAcht'),
(258, 0, 0, 124453, 0, 'int2', 'iZwei'),
(259, 0, 0, 124453, 0, 'val1', '8'),
(260, 0, 0, 124453, 0, 'val2', '2'),
(261, 0, 0, 124453, 0, 'val3', '6.0');

-- --------------------------------------------------------

--
-- Table structure for table `Veranstaltung`
--

CREATE TABLE `Veranstaltung` (
  `Stil` bigint NOT NULL DEFAULT '0',
  `Verantwortlich` varchar(255) NOT NULL DEFAULT '',
  `Mitarbeiter` varchar(255) NOT NULL DEFAULT '',
  `Titel` varchar(255) NOT NULL DEFAULT '',
  `ID` bigint NOT NULL,
  `Kopftext` varchar(255) NOT NULL DEFAULT '',
  `Fusstext` varchar(255) NOT NULL DEFAULT '',
  `Kurztext` varchar(20) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Veranstaltung`
--

INSERT INTO `Veranstaltung` (`Stil`, `Verantwortlich`, `Mitarbeiter`, `Titel`, `ID`, `Kopftext`, `Fusstext`, `Kurztext`) VALUES
(1, 'Friedrich-Schiller-Universität Jena \\\\\r\nFakultät für Mathematik und Informatik \\\\\r\nInstitut für Informatik \\\\\r\nProf. Dr. Wolfram Amme ', 'Dr. Sven Sickert \\\\\r\nM.Sc. Andr\\\'{e} Schäfer', '\\Large\r\nÜbung zur Vorlesung \\\\\r\n\\textbf{Objektorientierte Programmierung} \\\\\r\nSommersemester 2020', 158, 'Abgabe der Lösungen bis spätestens Freitag, 13 Uhr, im Moodle!', 'Viel Spaß und Erfolg !', 'OOP SoSe20 Übung');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Aufgaben`
--
ALTER TABLE `Aufgaben`
  ADD PRIMARY KEY (`ID`);
ALTER TABLE `Aufgaben` ADD FULLTEXT KEY `Name` (`Name`);
ALTER TABLE `Aufgaben` ADD FULLTEXT KEY `Titel` (`Titel`);

--
-- Indexes for table `Blatt`
--
ALTER TABLE `Blatt`
  ADD PRIMARY KEY (`ID`);


--
-- Indexes for table `Makro`
--
ALTER TABLE `Makro`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `Veranstaltung`
--
ALTER TABLE `Veranstaltung`
  ADD PRIMARY KEY (`ID`);

ALTER TABLE `Aufgaben`
  MODIFY `ID` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=500000;
  COMMIT;
-- AUTO_INCREMENT for dumped tables
ALTER TABLE `Blatt`
  MODIFY `ID` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=500000;
  COMMIT;
--
ALTER TABLE `Blatt`
  MODIFY `ID` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=500000;
  COMMIT;
--
-- AUTO_INCREMENT for table `Makro`
--
ALTER TABLE `Makro`
  MODIFY `ID` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=262;

--
-- AUTO_INCREMENT for table `Veranstaltung`
--
ALTER TABLE `Veranstaltung`
  MODIFY `ID` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;



-- creat a helper function to store the selected aufgaben;
-- this is for store the aufgaben for a blatt
CREATE TABLE helpers(
	id bigint NOT NULL AUTO_INCREMENT,
    aufgaben_id bigint,
    blatt_id bigint,
    point_a bigint NOT NULL,   
    order_a bigint NOT  NULL, 
    PRIMARY KEY(id),                                  
    FOREIGN KEY (aufgaben_id) REFERENCES Aufgaben(ID) ON DELETE CASCADE,
    FOREIGN KEY (blatt_id) REFERENCES Blatt(ID) ON DELETE CASCADE
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- this is for users' dashnboard
CREATE TABLE dashboard(
	id int NOT NULL AUTO_INCREMENT, 
    user_id bigint NOT NULL,
    veranSt_id bigint NOT NULL,
    is_owner TINYINT(1) DEFAULT 0,
    created_date DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id),  
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY (veranSt_id) REFERENCES Veranstaltung(ID) ON DELETE CASCADE
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- This table is for access level, right now the au
CREATE TABLE access_premission (
  id int NOT NULL AUTO_iNCREMENT,
  user_id bigint NOT NULL,
  aufgabe_id bigint NOT NULL DEFAULT 0,
  access_level VARCHAR (100) NOT NULL DEFAULT "full_access", -- levels: full_access , read_access, no_access
  PRIMARY KEY(id),  
  FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
  FOREIGN KEY (aufgabe_id) REFERENCES Aufgaben(ID) ON DELETE CASCADE

)ENGINE=InnoDB DEFAULT CHARSET=utf8;

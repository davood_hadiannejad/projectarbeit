import"../scss/main.scss";
import{ get } from"./apiwrapper";

// Global Variable to store the seleceted aufgaben
const addedAufgaben = [];

// Aufgabe Class: Represents Aufgabe 
export class Aufgabe{
  constructor( Name, aufgaben_id, point_a ){
    this.Name = Name;
    this.aufgaben_id = aufgaben_id;
    this.point_a = point_a;
  }
}

// Aufgaben UI class
export class UI{
  static displayAufgaben( ){
    const blatt_id = document.getElementById( "blatt_id" ).dataset.blatt_id;
    get( `/blatt/blatt_aufgaben/${blatt_id}` ).then( aufgaben => {
      if( aufgaben === undefined )
        return;
      aufgaben.forEach( aufgabe => {
        UI.addAufgabeToList( aufgabe );
      } );
    } );
  }
  static addAufgabeToList( aufgabe ){
    addedAufgaben.push( { 
      id:      aufgabe.aufgaben_id,
      point_a: aufgabe.point_a 
    } );

    // Get the table body by its class name
    const list = document.querySelector( "#aufgaben_list" );
    // create a row for the table 
    const row = document.createElement( "tr" );
    row.id = aufgabe.aufgaben_id;
    row.classList.add( "draggable" );
    row.draggable = "true";
    
    // create cells for the table 

    // name field 
    const td1 = document.createElement( "td" );
    const td1Text = document.createTextNode( aufgabe.Name );
    td1.appendChild( td1Text );
      
    // input field for points 
    const td2 = document.createElement( "td" );
    const point_a = document.createElement( "input" );
    point_a.value = aufgabe.point_a;
    point_a.type = "number";
    point_a.onchange = () => {
      for( let index = 0; index < addedAufgaben.length; index++ ) {
        const child = addedAufgaben[index];
        if( child.id === aufgabe.aufgaben_id )  
          child.point_a = point_a.value;
      }
    };      
    td2.appendChild( point_a );
      
    // delete button cell
    const td3 = document.createElement( "td" );
    const deleteBtn = document.createElement( "button" );
    deleteBtn.classList.add( "btn", "btn-danger", "btn-sm", "delete" );
    deleteBtn.appendChild( document.createTextNode( "X" ) );
    td3.appendChild( deleteBtn );
      
    row.appendChild( td1 );
    row.appendChild( td2 );
    row.appendChild( td3 );
      
    list.appendChild( row );
  }
    
  // Create custom alert 
  static showAlert( message, className ) {
    const div = document.createElement( "div" );
    div.className = `alert alert-${className}`;
    div.appendChild( document.createTextNode( message ) );
    const form = document.querySelector( "#aufgaben_form" );
    const parentDiv = document.querySelector( "#aufgaben_form" ).parentNode;
    parentDiv.insertBefore( div, form );
  
    // Vanish in 3 seconds
    setTimeout( () => document.querySelector( ".alert" ).remove(), 3000 );
  }
    
  static deleteAufgabe( element ){
    if( element.classList.contains( "delete" ) ){
      const deleted_element = element.parentElement.parentElement;
      for( let index = 0; index < addedAufgaben.length; index++ ) {
        const child = addedAufgaben[index];
        if( child.id == deleted_element.id ) 
          addedAufgaben.splice( index, 1 );
      }
      deleted_element.remove();

      // Show success message 
      UI.showAlert( "Aufgabe Removed", "success" );
    }
  }
  
  static getDragAfterElement( table_body, y ){
    const draggable_elements = [...table_body.querySelectorAll( ".draggable:not(.dragging)" )]; 
    return draggable_elements.reduce( ( closest, child ) => {
      const box = child.getBoundingClientRect();
      const offset = y - box.top - box.height / 2;
      if( offset < 0 && offset > closest.offset ) 
        return {
          offset:  offset,
          element: child 
        };
      else 
        return closest;
    }, { offset: Number.NEGATIVE_INFINITY } ).element;
  }
  
  // Get the table data 
  static getAufgabenFromTable( ){
    // select the table body for events 
    const element = document.querySelector( "#aufgaben_list" );
    const rows = element.rows;
    for( let index = 0; index < rows.length; index++ ) {
      const id = rows[index].id ;
      const point_a = element.rows[index].cells[1].querySelector( "input" ).value;
      addedAufgaben[index].id = id;
      addedAufgaben[index].point_a = point_a;
    }
    
    return addedAufgaben;
  }

  static dragEvents( ){
    // select the table body for events 
    const table_body = document.querySelector( "#aufgaben_list" );

    // Event: Add dragging class
    table_body.addEventListener( "dragstart", event => {
      event.target.classList.add( "dragging" );
    } );

    // Event: Remove dragging class 
    table_body.addEventListener( "dragend", event => {
      event.target.classList.remove( "dragging" );
      UI.getAufgabenFromTable( );
    } );

    // Event: Drag an Aufgabe 
    table_body.addEventListener( "dragover", event => { 
      event.preventDefault();
      const after_element = UI.getDragAfterElement( table_body, event.clientY );
      const draggable = document.querySelector( ".dragging" );
      table_body.insertBefore( draggable, after_element );
    } );
  }
}
  

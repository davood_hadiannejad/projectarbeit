
const sendHttpRequest = ( method, url, data ) => {
  return fetch( `${window.origin}${url}`, {
    method:      method,
    redirect:    "follow",
    credentials: "include",
    cache:       "no-cache",
    body:        JSON.stringify( data ),
    headers:     data ? { "Content-Type": "application/json" } : {}
  } ).
    then( response => {
      if( response.status >= 400 )
        return response.json().then( errorData => {
          const error = new Error( "Looks like there was a problem!" );
          error.data = errorData;
          throw error;
        } );
        
      return response.json();
    } );
};

// GET Call
export function get( url ){
  return sendHttpRequest( "GET", url ).
    then( responseData => {
      return responseData;
    } ).catch( error => {
      console.log( error, error.data );
    } );
}

// POST Call
export function post( url, data, location ){
  console.log( url );
  return sendHttpRequest( "POST", url, data ).
    then( responseData => {
      window.location.href = location;
      return responseData.json();
    } ).
    catch( error => {
      console.log( error, error.data );
    } );
}

// PUT Call 
export function put( url, data ){
  sendHttpRequest( "PUT", url, data ).then( responseData => {
    console.log( responseData );
  } ).catch( error => {
    console.log( error, error.data );
  } );
}

// DELETE Call
export function remove( url ){
  return sendHttpRequest( "DELETE", url ).then( responseData => {
    return responseData;
  } ).catch( error => {
    console.log( error, error.data );
  } );
}

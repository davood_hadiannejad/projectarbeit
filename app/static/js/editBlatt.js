import{ BlattHelper } from"./blattHelper";
import{ UI } from"./UI";
import{
  post, remove 
} from"./apiwrapper";

window.addEventListener( "load", event => {
  // Prevent actual submit 
  event.preventDefault();

  UI.displayAufgaben( );

  BlattHelper.events();
} );

//  Add Aufgaben 
document.querySelector( "#aufgaben_form" ).onsubmit = function submint_aufgaben( event ) {
  // Prevent actual submit 
  event.preventDefault();

  const blatt_id = parseInt( document.getElementById( "blatt_id" ).dataset.blatt_id );
  const veranstaltung = parseInt( document.getElementById( "veranstaltung" ).dataset.veranstaltung );

  const addedAufgaben = UI.getAufgabenFromTable();
  const blattForm = {
    BlattId:       blatt_id,
    Nr:            document.getElementById( "Nr" ).value,
    Name:          document.getElementById( "Name" ).value,
    Titel:         document.getElementById( "Titel" ).value,
    Ausgabe:       document.getElementById( "Ausgabe" ).value,
    Abgabe:        document.getElementById( "Abgabe" ).value,
    Zusatz:        document.getElementById( "Zusatz" ).value,
    Stil:          document.getElementById( "Stil" ).value,
    Veranstaltung: veranstaltung,
    addedAufgaben: addedAufgaben
        
  };
  for( let index = 0; index < addedAufgaben.length; index++ ) {
    const aufgabe = addedAufgaben[index];
    if( aufgabe.point_a === "" ) {
      UI.showAlert( "Please fill in the point field", "danger" ); 
      return;
    }
  }
  
  post( `/dashboard/veranstaltungs/blatt/edit/${blatt_id}/${veranstaltung}`,
    blattForm, `/dashboard/veranstaltungs/blatt/${veranstaltung}` );

  UI.showAlert( "Aufgabe Edited ", "success" );
};

// select the table body 
const table_body = document.querySelector( "#aufgaben_list" );
// Event: Remove an Aufgabe 
table_body.addEventListener( "click", event=>{
  event.preventDefault();
  
  // Remove Aufgabe from UI
  UI.deleteAufgabe( event.target );
  
  if( event.target.classList.contains( "delete" ) ){
    const deleted_element = event.target.parentElement.parentElement;
    // Remove Aufgabe from Database
    remove( `/edit_blatt/delete_aufgaben/${deleted_element.id}` );
  }
} );

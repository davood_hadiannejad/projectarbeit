import{ 
  get, post 
} from"./apiwrapper";

window.addEventListener( "load", event => {
  event.preventDefault();
  const access_level = document.querySelectorAll( ".aufgabe_access" );
  const aaccess_enum = [ "full_access", "read_access", "no_access" ];

  get( "/admin/aufgaben_access" ).then( data => {
    for( let i = 0; i < data.length; i++ )
      access_level.forEach( element => {
        if( data[i].user_id === parseInt( element.id ) )
          element.options[aaccess_enum.indexOf( data[i].access_level )].selected = true;
      } );
  } );

  access_level.forEach( element => {
    element.addEventListener( "change", () => { 
      for( let i = 0; i < element.options.length; i++ ) {
        const opt = element.options[i];
        if( opt.selected ) {
          const data = {
            user_id:      element.id,
            access_level: opt.value
          };
          post( "/admin/update_access_level", data, "/admin" );
          return;
        }
      }
    } );
  } );
} );

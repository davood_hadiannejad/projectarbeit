import{ BlattHelper } from"./blattHelper";
import{ UI } from"./UI";
import{ post } from"./apiwrapper";

window.addEventListener( "load", event =>{
  event.preventDefault();
  BlattHelper.events();
} );

// Event: add  aufgaben 
document.querySelector( "#aufgaben_form" ).onsubmit = function submint_aufgaben( event ) {
  event.preventDefault();
  const veranstaltung = parseInt( document.getElementById( "veranstaltung" ).dataset.veranstaltung );
  const addedAufgaben = UI.getAufgabenFromTable();
  const blattForm = {
    Nr:            document.getElementById( "Nr" ).value,
    Name:          document.getElementById( "Name" ).value,
    Titel:         document.getElementById( "Titel" ).value,
    Ausgabe:       document.getElementById( "Ausgabe" ).value,
    Abgabe:        document.getElementById( "Abgabe" ).value,
    Zusatz:        document.getElementById( "Zusatz" ).value,
    Stil:          document.getElementById( "Stil" ).value,
    Veranstaltung: veranstaltung,
    addedAufgaben: addedAufgaben
          
  };
  for( let index = 0; index < addedAufgaben.length; index++ ) {
    const aufgabe = addedAufgaben[index];
    if( aufgabe.point_a === "" ) {
      UI.showAlert( "Please fill in the point field", "danger" ); 
      return;
    }
  }

  post( `/dashboard/veranstaltungs/blatt/add/${veranstaltung}`, 
    blattForm, `/dashboard/veranstaltungs/blatt/${veranstaltung}` );

  UI.showAlert( "Aufgabe Added", "success" );
};

// select the table body 
const table_body = document.querySelector( "#aufgaben_list" );
// Event: Remove an Aufgabe 
table_body.addEventListener( "click", event=>{
  // Remove Aufgabe from UI
  UI.deleteAufgabe( event.target );
} );

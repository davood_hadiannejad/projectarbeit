import{
  UI, Aufgabe 
} from"./UI";

import{ get } from"./apiwrapper";

export class BlattHelper{
  static events(){
    const aufgaben = document.getElementById( "select_aufgaben" );
    get( "/dashboard/veranstaltungs/blatt/add/aufgaben" ).then( data => {
      BlattHelper.options( aufgaben, data );
    } );
  
    // Event Add a Augabe 
    aufgaben.addEventListener( "change", ()=> {
      const Name = aufgaben.options[aufgaben.selectedIndex].text;
      const aufgaben_id = aufgaben.options[aufgaben.selectedIndex].value;
      const point_a = "";

      // Instatiate Aufgabe
      const aufgabe = new Aufgabe( Name, aufgaben_id, point_a );
    
      // Add Aufgabe to UIs
      UI.addAufgabeToList( aufgabe ); 
    } );

    // Drag function
    UI.dragEvents( );
  }

  // Selection options
  static options( element, data ){
    for( let i = 0; i < data.length; i++ ){
      let opt = document.createElement( "option" );
      opt.value = data[i].ID;
      opt.text = data[i].Titel;
      element.appendChild( opt );
    }
  }
}


import{
  get, post 
} from"./apiwrapper";

const users = document.getElementById( "select_user" );
get( "/dashboard/veranstaltungs/users/all" ).then( data => {
  for( let i = 0; i < data.length; i++ ){
    let opt = document.createElement( "option" );
    opt.value = data[i].id;
    opt.text = `${data[i].first_name} ${data[i].last_name}`;
    users.appendChild( opt );
  }
} );

const veranstaltung = document.getElementById( "veranstaltung" ).dataset.veranstaltung;
// IF any user selected 
users.addEventListener( "change", () => {
  const data = {
    user_id:    users.options[users.selectedIndex].value,
    veranSt_id: veranstaltung

  };
  post( "/dashboard/veranstaltungs/users/assign", 
    data, `/dashboard/veranstaltungs/users/${veranstaltung}` );
} );

# Adele2

### Installations

#### we are aging to use flask, javascript, mysql and pylatex, go ahead the followings:

### Please do These first:

####  Please read the [Flask Documentation](https://flask.palletsprojects.com/en/1.1.x/installation/),
####  Please read the [Flask Blueprints](https://exploreflask.com/en/latest/blueprints.html).
####  Please read the [Webpack](https://webpack.js.org/configuration/),
####  Please fide a Mysql Documentation for yourself, 
#### Also have a look at [Pylatex Documentation](https://jeltef.github.io/PyLaTeX/current/)

#### And: 

#### clone the project
> \$ cd projectFolder


#### Create A Python  Environment

> \$ pip install --upgrade pip

> \$ python3 -m venv env (linux)

> \$ py -3 venv venv (windows)

#### Activate The Enviroment ( every time you open a new terminal you will need to active this and if you want to install anything it has to be done in this enviroment )

> \$ source env/bin/activate (Linux)

> \$ souce venv/Scripts/activate (windows)

#### And all you need to do:

> \$ pip install -r requirements.txt (this will install all python flask dependencies)

> \$ npm install  (this will install all the javascript dependencies)

#### And install mysql please, import the mysql tables as written in Adele.sql 
#### to avoid FOREIGN KEY Errors do this first: "SET FOREIGN_KEY_CHECKS=0; " and then again "SET FOREIGN_KEY_CHECKS=1;"

#### And set the config.py file in root folder

#### And compile the javascript and scss files:

>  \$ npm run watch (For Development mode )

>  \$ npm run build ( For Deployment mode  )

#### To run the app:
#### first set the app mode :
> \$ export FLASK_ENV=development

#### For Production Mode See [Documentation](https://flask.palletsprojects.com/en/1.1.x/tutorial/deploy/)

#### and simple:

> \$ flask run 

##### To deactivate the virtual enviroment:

>  \$ deactivate

##### or just close your terminal :)


#### If we want to install anything manually do like below (for example if pip install -r requirements.txt doesn't work on your system or  you want to install new pakages )
#### just keep in mind you should be in the app's virtual environment that you activate already

#### Examples: 

##### install Flask 
> \$ pip install Flask 


##### install flask-mysqldb (You need also to install MySql on your machine )

> \$ pip install flask_mysqldb


##### Install WTforms

> \$ pip install Flask-WTF


##### install passlib for password hashing

> \$ pip install passlib

#### And for installing the frontend dependencies

#### for example to install file loader
> npm install file-loader --save-dev

#### BUT Please read first its documentions and never install anything globally on your machine except node js, 
#### Every Web dependencies should be saved like above  


# App Structures

##### run.py : this main file

##### requirements.txt : dependencies (python)
##### package-lock.json: dependencies (frontend)
##### webpack: this will compile the js and scss files
##### .eslintrc.js: this is for formatting and linting (I use it with vscode but it can be configured with any of the editors even with webpack). And all its rules have been written by myself. feel free if want to use it and also if you want to change the rules. )

##### config.py: this allows us to create a configuration file, where we can place all of our config values which alows us to control the config environment outside the application by setting the FLASK_ENV environment variable in the terminal

#### textfiles directory: textfiles directory: this will contain the latex output files (  every time a new latex is compiled, the old latex files will be deleted). And also inside this folder, there is another folder called img, this is for Aufgaben images and never has to be deleted. 

#### app folder : this is going to contain our Flask application and become our package


#### inside the app folder:

##### **init**.py : Think of the **init**.py file as a constructor that pulls all of the parts of our application together into a package and then tells Python to treat it as a package!

##### views : this will contain our URLs

##### templates folder: Flask looks for a directory called templates in the root of the Flask application package. Will contain all of the HTML files we want to serve from views

##### flask also requires a static directory, which will contain our css and javascript files
##### inside the static folder there is js folder which contain all javascript and scss files 
###### Please nod the js and scss files need to be compiled, this can be done using the webpack just run:
> \$ npm run watch (For Development mode )

>  \$ npm run build ( For Deployment mode  )

##### After compiling will be created a dist folder inside the static folder, which contains our js and scss compiled files.

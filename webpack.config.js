const webpack = require( "webpack" );
const path = require( "path" );
const MiniCssExtractPlugin = require( "mini-css-extract-plugin" );
const CircularDependencyPlugin = require( "circular-dependency-plugin" );
const CssnanoPlugin = require( "cssnano-webpack-plugin" );
const CssMinimizerPlugin = require( "css-minimizer-webpack-plugin" );
const CleanWebpackPlugin = require( "clean-webpack-plugin" );

module.exports = {
  name:    process.env.NODE_ENV == "production" ? "production" : "development",
  mode:    process.env.NODE_ENV == "production" ? "production" : "development",
  // devtool: "eval-source-map",
  context: path.resolve( __dirname, "app" ),
  entry:   {
    addBlatt:     path.resolve( __dirname + "/app/static/js/addBlatt.js" ),
    editBlatt:    path.resolve( __dirname + "/app/static/js/editBlatt.js" ),
    veranStUsers: path.resolve( __dirname + "/app/static/js/veranStUsers.js" ),
    admin:        path.resolve( __dirname + "/app/static/js/admin.js" )
  },
    
  module: { rules: [
    {
      test:    /\.js$/,
      exclude: /node_modules/,
      loaders: ["babel-loader"]
      
    },
    {
      test:    /\.(scss|css)$/,
      exclude: /node_modules/,
      use:     [
        {
          loader:  MiniCssExtractPlugin.loader,
          options: {
            reloadAll: true,
            sourceMap: true
          }
        },
        "css-loader",
        "postcss-loader",
        "sass-loader"
      ]
    },

    {
      test:    /\.(png|gif|jpg|cur)$/i,
      loader:  "url-loader",
      options: { limit: 8192 }
    }

  ] },
  resolve: { extensions: [ ".js", "json", ".css", ".scss" ] },
  output:  {
    publicPath: path.resolve( __dirname, "./app/static/dist" ),
    filename:   "[name].js",
    path:       path.resolve( __dirname, "./app/static/dist" )
  },
  optimization: {
    minimize:  true,
    minimizer: [
      // new CssMinimizerPlugin(),
      new CssnanoPlugin( { sourceMap: true } )
    ]
  },

  plugins: [
    new MiniCssExtractPlugin( { filename: "../dist/main.css" } ),
    new CircularDependencyPlugin( {
      exclude:     /a\.js|node_modules/,
      failOnError: true,
      cwd:         process.cwd()
    } )
  ]

};

# App configuration file
# flask allows us to create a configuration file,
# where we can place all of our config values,
# overwrite the default values and also create our own


class Config(object):
    DEBUG = False
    TESTING = False

    MYSQL_HOST = 'localhost'
    MYSQL_USER = 'davoo'
    MYSQL_PASSWORD = 'Admin@123'
    MYSQL_DB = 'myflaskapp'
    MYSQL_CURSORCLASS = 'DictCursor'
    SECRET_KEY = 'secret123'
    SESSION_COOKIE_SECURE = True


# this is the config class will be used for running in production mode
class ProductionConfig(Config):
    pass


# this is the config class will be used for running in development mode
class DevelopmentConfig(Config):
    DEBUG = True
    MYSQL_HOST = 'localhost'
    MYSQL_USER = 'davoo'
    MYSQL_PASSWORD = 'Admin@123'
    MYSQL_DB = 'myflaskapp'

    SESSION_COOKIE_SECURE = False


# this is the config class will be used for testing
class TestingConfig(Config):
    TESTING = True

    MYSQL_HOST = 'localhost'
    MYSQL_USER = 'davoo'
    MYSQL_PASSWORD = 'Admin@123'
    MYSQL_DB = 'myflaskapp'

    SESSION_COOKIE_SECURE = True
